const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  theme: {
    extend: {
      height: {
        '7': '1.75rem',
        '9': '2.25rem',
        '80': '20rem',
        '125': '31.25rem',
      },
      width: {
        '9': '2.25rem',
        '100': '25rem',
        '120': '30rem',
        '150': '37.5rem',
        '200': '50rem',
        '333': '83.125rem',
        '6/10': '60%',
        '4/10': '40%',
      },
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      fontSize: {
        '5xl-plus': '3.5rem',
        '7xl': '5rem',
      },
      borderRadius: {
        xl: '1rem',
      },
      colors: {
        cyan: {
          '100': '#E6F5FF',
          '300': '#63B3ED',
          '400': '#1A9EFE',
        },
      },
      boxShadow: {
        'all-md': '0px 4px 10px rgba(0, 0, 0, 0.15)',
        all: '0px 4px 20px rgba(0, 0, 0, 0.15)',
      },
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1440px',
      '3xl': '1536px',
      '4xl': '1920px',
    },
  },
  variants: {
    backgroundColor: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
    gradientColorStops: [
      'responsive',
      'hover',
      'focus',
      'active',
      'group-hover',
    ],
  },
  purge: {
    content: [
      './app/**/*.php',
      './resources/**/*.html',
      './resources/**/*.js',
      './resources/**/*.jsx',
      './resources/**/*.ts',
      './resources/**/*.tsx',
      './resources/**/*.php',
      './resources/**/*.vue',
      './resources/**/*.twig',
    ],
    options: {
      defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
      whitelistPatterns: [/-active$/, /-enter$/, /-leave-to$/, /show$/],
    },
  },
  plugins: [require('@tailwindcss/ui'), require('@tailwindcss/typography')],
};
