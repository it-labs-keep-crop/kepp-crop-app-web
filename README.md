# KeepCrop

El sistma keepcrop permite planificar el cuidado de tus cultivos de manera óptima

## Requerimientos

- PostgreSQL 9 o Superior
- Php 7.2 o superior
- [Laravel 8](https://laravel.com/docs/8.x/installation)
- [Composer](https://getcomposer.org/download/)

## Instalación del proyecto

- Luego de clonar el repositorio, copiar archivo con variables de entorno

  ```
  $ cp .env.example .env
  ```

- Instalar dependencias del proyecto via Composer

  ```
  $ composer install
  ```

- Generar llave de aplicación

  ```
  $ php artisan key:generate
  ```

- Editar variables de entorno importantes a notar y editar son:

  ```ENV
  [Conexión a base de datos]
  DB_CONNECTION=pgsql
  DB_HOST=127.0.0.1
  DB_PORT=5432
  DB_DATABASE=nombre-base-datos   # editar
  DB_USERNAME=username            # editar
  DB_PASSWORD=password            # editar

  [Para notificaciones vía correo]
  # Editar según servicio
  MAIL_DRIVER=
  MAIL_HOST=
  MAIL_PORT=
  MAIL_USERNAME=
  MAIL_PASSWORD=
  MAIL_FROM_ADDRESS=
  MAIL_FROM_NAME=
  ```

- Migraciones y semillas para base de datos

  ```
  $ php artisan migrate --seed
  ```
