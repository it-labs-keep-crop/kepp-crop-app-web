<?php

namespace App\Http\Livewire;

use App\TipoTarea;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class TipoTareaTable extends DataTableComponent
{
    public string $searchLabel = 'Buscar';
    public bool $showSorting = false;
    public string $emptyMessage = 'No se encontraron elementos que coincidan con tu búsqueda.';

    public function columns(): array
    {
        return [
            Column::make('Titulo')
                ->sortable()
                ->searchable(),
            Column::make('', 'id')
                ->format(function($value) {
                    return '<a href="'. route('tipotareas.detail', ['tipoTarea' => $value]) .'" class="text-indigo-500 hover:text-indigo-700">Ver</a>';
                })
                ->asHtml(),
        ];
    }

    public function query(): Builder
    {
        return TipoTarea::query();
    }
}
