<?php

namespace App\Http\Livewire;

use App\TipoSemilla;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class TipoSemillaTable extends DataTableComponent
{
    public string $searchLabel = 'Buscar';
    public bool $showSorting = false;
    public string $emptyMessage = 'No se encontraron elementos que coincidan con tu búsqueda.';

    public function columns(): array
    {
        return [
            Column::make('Nombre')
                ->sortable()
                ->searchable(),
            Column::make('Publicado')
                ->format(function($value) {
                  if ($value) {
                    return '<span class="inline-flex px-3 py-1 font-semibold text-green-800 bg-green-100 rounded-full text-s leading-5">
                      Publicado
                      </span>';
                  }
                  return '<span class="inline-flex px-3 py-1 font-semibold text-indigo-800 bg-indigo-200 rounded-full text-s leading-5">
                    Borrador
                    </span>';
                })
                ->asHtml(),
            Column::make('Ciclo de vida (días)', 'ciclo_vida')
                ->sortable(),
            Column::make('Curado recomendado (C°)', 'temperatura_curado_recomendado')
                ->sortable(),
            Column::make('Tiempo de curado (días)', 'tiempo_curado'),
            Column::make('', 'id')
                ->format(function($value) {
                    return '<a href="'. route('tiposemillas.detail', ['tipoSemilla' => $value]) .'" class="text-indigo-500 hover:text-indigo-700">Ver</a>';
                })
                ->asHtml(),
        ];
    }

    public function query(): Builder
    {
        return TipoSemilla::query();
    }
}
