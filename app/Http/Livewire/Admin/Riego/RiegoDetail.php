<?php

namespace App\Http\Livewire\Admin\Riego;

use Carbon\Carbon;
use App\TipoSemilla;
use Livewire\Component;

class RiegoDetail extends Component
{
  public $breadcrumb;
  public $tipoSemilla;

  public $frecuencia_regado;
  public $frecuencia_fertilizante;

  public $today;
  public $loading;
  public $tempDate;
  public $dayCounter;

  public $rules = [
    'frecuencia_regado' => 'required',
    'frecuencia_fertilizante' => 'required',
  ];

  public function mount(TipoSemilla $tipoSemilla)
  {
    $this->breadcrumb = [
      route('regado') => 'Regado',
      '' => 'Detalle',
    ];

    $this->tipoSemilla = $tipoSemilla;
    $this->fillWithModel();

    $this->loading = true;
    $this->setCalendar();
  }

  public function setCalendar()
  {
    $this->dayCounter = 0;
    $this->today = Carbon::today()->locale('es_CL');
    $this->tempDate = Carbon::createFromDate($this->today->year, $this->today->month, 1);

    $skip = $this->tempDate->dayOfWeekIso;
    for ($i = 1; $i < $skip; $i++) $this->tempDate->subday();

    $this->loading = false;
  }

  public function updated($name, $value)
  {
    if ($name == 'frecuencia_fertilizante' || $name == 'frecuencia_regado') {
      $this->loading = true;
      if (is_numeric($value) && $value >= 0) {
        $this->setCalendar();
      }
    }
  }

  public function render()
  {
    return view('livewire.admin.riego.riego-detail')
      ->layout('admin-dashboard');
  }

  public function cancel()
  {
    $this->fillWithModel();
    $this->setCalendar();
    $this->dispatchBrowserEvent('stop-edit');
  }

  public function store()
  {
    $data = $this->validate();

    $this->tipoSemilla->fill($data);
    $this->tipoSemilla->save();

    session()->flash('success', 'Regado actualizado correctamente');
    return redirect()->route('regado.detail', ['tipoSemilla' => $this->tipoSemilla->id]);
  }

  protected function fillWithModel()
  {
    $this->fill([
      'frecuencia_regado' => $this->tipoSemilla->frecuencia_regado,
      'frecuencia_fertilizante' => $this->tipoSemilla->frecuencia_fertilizante,
    ]);
  }
}
