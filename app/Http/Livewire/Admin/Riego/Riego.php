<?php

namespace App\Http\Livewire\Admin\Riego;

use App\TipoSemilla;
use Livewire\Component;
use Livewire\WithPagination;

class Riego extends Component
{
  use WithPagination;

  public $q;

  protected $queryString = ['q'];

  public function mount()
  {
    $this->q = request()->query('q', $this->q);
  }

  public function render()
  {
    $tipos = TipoSemilla::where('nombre', 'ilike', '%'.$this->q.'%')->orderBy('nombre')
            ->paginate(10);
    $breadcrumb = ['' => 'Regado'];
    return view('livewire.admin.riego.riego', compact('tipos', 'breadcrumb'))
      ->layout('admin-dashboard');
  }
}
