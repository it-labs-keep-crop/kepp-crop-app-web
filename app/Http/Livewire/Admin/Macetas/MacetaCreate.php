<?php

namespace App\Http\Livewire\Admin\Macetas;

use App\Maceta;
use Livewire\Component;

class MacetaCreate extends Component
{
    public $breadcrumb;

    public $nombre;
    public $capacidad_litros;
    public $agua;
    public $descripcion;
    public $dimensiones = [
      'alto' => null,
      'ancho' => null,
      'largo' => null,
    ];

    public $rules = [
        'nombre' => 'required',
        'capacidad_litros' => 'required|numeric',
        'agua' => 'required|numeric',
        'dimensiones.alto' => 'required|numeric',
        'dimensiones.ancho' => 'required|numeric',
        'dimensiones.largo' => 'required|numeric',
        'descripcion' => 'required|max:254',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('macetas') => 'Macetas',
            '' => 'Crear maceta',
        ];
    }

    public function render()
    {
        return view('livewire.admin.macetas.maceta-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('macetas');
    }

    public function store()
    {
        $data = $this->validate();
        $data['dimensiones'] = implode("x", collect($this->dimensiones)->values()->toArray());

        Maceta::create($data);

        session()->flash('success', 'Maceta creada correctamente');
        return redirect()->route('macetas');
    }
}
