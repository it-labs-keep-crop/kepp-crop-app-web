<?php

namespace App\Http\Livewire\Admin\Macetas;

use Livewire\Component;

class Macetas extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Macetas'];

        return view('livewire.admin.macetas.macetas', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
