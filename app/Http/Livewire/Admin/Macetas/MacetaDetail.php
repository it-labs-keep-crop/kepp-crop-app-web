<?php

namespace App\Http\Livewire\Admin\Macetas;

use App\Maceta;
use Livewire\Component;

class MacetaDetail extends Component
{
    public $maceta;
    public $breadcrumb;

    public $nombre;
    public $capacidad_litros;
    public $agua;
    public $dimensiones;
    public $descripcion;

    public $rules = [
        'nombre' => 'required',
        'capacidad_litros' => 'required|numeric',
        'agua' => 'required|numeric',
        'dimensiones' => 'required',
        'descripcion' => 'required|max:254',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(Maceta $maceta)
    {
        $this->breadcrumb = [
            route('macetas') => 'Macetas',
            '' => 'Detalle',
        ];
        $this->maceta = $maceta;
        $this->fillWithModel();
    }

    public function render()
    {
        return view('livewire.admin.macetas.maceta-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function storeMaceta()
    {
        $data = $this->validate();
        $data['dimensiones'] = implode("x", collect($dimensiones)->values()->toArray());
        $this->maceta->fill($data)->save();

        session()->flash('success', 'Maceta editada correctamente');
        return redirect()->route('macetas.detail', ['maceta' => $this->maceta->id]);
    }

    public function destroy()
    {
        $this->maceta->delete();
        session()->flash('success', 'Maceta eliminada correctamente');

        return redirect()->route('macetas');
    }

    protected function fillWithModel()
    {
        $d = explode("x", $this->maceta->dimensiones);

        $this->fill([
            'nombre' => $this->maceta->nombre,
            'capacidad_litros' => $this->maceta->capacidad_litros,
            'agua' => $this->maceta->agua,
            'dimensiones' => [
              'alto' => $d[0],
              'ancho' => $d[1],
              'largo' => $d[2],
            ],
            'descripcion' => $this->maceta->descripcion,
        ]);
    }
}
