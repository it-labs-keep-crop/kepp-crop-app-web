<?php

namespace App\Http\Livewire\Admin\Proyectos;

use Livewire\Component;

class Proyectos extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Proyectos'];

        return view('livewire.admin.proyectos.proyectos', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
