<?php

namespace App\Http\Livewire\Admin\Proyectos;

use App\Etapa;
use App\Proyecto;
use App\TipoSemilla;
use App\User;
use Livewire\Component;

class ProyectoCreate extends Component
{
    public $breadcrumb;
    public $tiposSemilla;
    public $etapas;

    public $etapa_id;
    public $nombre;
    public $tipo_cultivo;
    public $tipo_semilla_id;

    public $rules = [
        'nombre' => 'required',
        'tipo_cultivo' => 'required',
        'tipo_semilla_id' => 'required|exists:tipo_semillas,id',
        'etapa_id' => 'required|exists:etapas,id',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('proyectos') => 'Proyectos',
            '' => 'Crear proyecto',
        ];
        $this->tiposSemilla = TipoSemilla::select('id', 'nombre')->get();
        $this->etapas = Etapa::select('id', 'nombre')->get();
        $this->tipo_semilla_id = $this->tiposSemilla->first()->id;
        $this->etapa_id = $this->etapas->first()->id;
    }

    public function render()
    {
        return view('livewire.admin.proyectos.proyecto-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('proyectos');
    }

    public function store()
    {
        $data = $this->validate();
        // TODO asignar user/adminUser segun logica de aplicacion
        $data['user_id'] = User::first()->id;
        Proyecto::create($data);

        session()->flash('success', 'Proyecto creado correctamente');
        return redirect()->route('proyectos');
    }
}
