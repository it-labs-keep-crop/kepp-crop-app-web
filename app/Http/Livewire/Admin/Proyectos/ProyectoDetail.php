<?php

namespace App\Http\Livewire\Admin\Proyectos;

use App\Etapa;
use App\Proyecto;
use App\TipoSemilla;
use App\User;

use Livewire\Component;

class ProyectoDetail extends Component
{
    public $proyecto;
    public $breadcrumb;
    public $tiposSemilla;
    public $etapas;

    public $nombre;
    public $tipo_cultivo;
    public $tipo_semilla_id;
    public $etapa_id;
    public $user_id;

    public $rules = [
        'nombre' => 'required',
        'tipo_cultivo' => 'required',
        'tipo_semilla_id' => 'required|exists:tipo_semillas,id',
        'etapa_id' => 'required|exists:etapas,id',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(Proyecto $proyecto)
    {
        $this->breadcrumb = [
            route('proyectos') => 'Proyectos',
            '' => 'Detalle',
        ];
        $this->proyecto = $proyecto->load(['tipoSemilla', 'etapa', 'user']);
        $this->fillWithModel();

        $this->tiposSemilla = TipoSemilla::select('id', 'nombre')->get();
        $this->etapas = Etapa::select('id', 'nombre')->get();
    }

    public function render()
    {
        return view('livewire.admin.proyectos.proyecto-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();
        $this->proyecto->fill($data)->save();

        session()->flash('success', 'Proyecto editado correctamente');
        return redirect()->route('proyectos.detail', ['proyecto' => $this->proyecto->id]);
    }

    public function destroy()
    {
        $this->proyecto->delete();
        session()->flash('success', 'Proyecto eliminado correctamente');

        return redirect()->route('proyectos');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'nombre' => $this->proyecto->nombre,
            'tipo_cultivo' => $this->proyecto->tipo_cultivo,
            'tipo_semilla_id' => $this->proyecto->tipo_semilla_id,
            'etapa_id' => $this->proyecto->etapa_id,
            'user_id' => $this->proyecto->user_id,
        ]);
    }
}
