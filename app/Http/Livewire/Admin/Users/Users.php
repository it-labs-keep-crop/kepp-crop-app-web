<?php

namespace App\Http\Livewire\Admin\Users;

use Livewire\Component;

class Users extends Component
{
  public function render()
  {
    $breadcrumb = ['' => 'Equipo'];

    return view('livewire.admin.users.users', compact('breadcrumb'))
      ->layout('admin-dashboard');
  }
}
