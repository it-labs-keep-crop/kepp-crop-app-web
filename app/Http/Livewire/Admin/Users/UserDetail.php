<?php

namespace App\Http\Livewire\Admin\Users;

use App\AdminUser;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;

class UserDetail extends Component
{
    use WithFileUploads;

    public $user;
    public $breadcrumb;
    public $isLogged;

    public $name;
    public $email;
    public $avatar;

    public $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'avatar' => 'nullable|sometimes|image',
    ];

    public $listeners = [
      'accept-modal' => 'destroy',
    ];

    public function mount(AdminUser $adminUser)
    {
        $this->breadcrumb = [
            route('users') => 'Equipo',
            '' => 'Detalle',
        ];
        $this->isLogged = $adminUser->id == Auth::user()->id;

        $this->user = $adminUser;
        $this->name = $this->user->name;
        $this->email = $this->user->email;
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required',
            'email' => 'email',
        ]);
    }

    public function render()
    {
        return view('livewire.admin.users.user-detail')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('users.detail', ['adminUser' => $this->user->id]);
    }

    public function storeUser()
    {
        $data = $this->validate();
        if ($this->avatar) {
            $data['avatar'] = str_replace('public/', 'storage/', $this->avatar->store('public/avatars'));
        } else {
            unset($data['avatar']);
        }
        $this->user->fill($data)->save();

        session()->flash('success', 'Usuario actualizado correctamente');
        return redirect()->route('users.detail', ['adminUser' => $this->user->id]);
    }

    public function destroy()
    {
        //todo -> check if deleted user == logged user
        $this->user->delete();
        session()->flash('success', 'Usuario eliminado correctamente');

        return redirect()->route('users');
    }
}
