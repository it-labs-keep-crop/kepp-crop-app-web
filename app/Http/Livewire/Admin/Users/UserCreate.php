<?php

namespace App\Http\Livewire\Admin\Users;

use App\AdminUser;
use Livewire\Component;
use Livewire\WithFileUploads;

class UserCreate extends Component
{
    use WithFileUploads;

    public $title;
    public $name;
    public $email;
    public $avatar;
    public $password;
    public $password_confirmation;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'email' => 'required|email',
        ]);
    }

    public function render()
    {
        $this->title = 'Crear Usuario';
        return view('livewire.admin.users.user-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('users');
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|same:password_confirmation|min:6',
            'avatar' => 'nullable|image',
        ]);
        $path = $this->avatar ? str_replace('public/', 'storage/', $this->avatar->store('public/avatars')) : null;

        AdminUser::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->password),
            'avatar' => $path,
        ]);

        session()->flash('success', 'Usuario creado correctamente');
        return redirect()->route('users');
    }
}
