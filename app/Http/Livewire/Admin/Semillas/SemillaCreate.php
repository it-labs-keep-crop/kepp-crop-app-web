<?php

namespace App\Http\Livewire\Admin\Semillas;

use App\Marca;
use App\Semilla;
use App\TipoSemilla;
use Livewire\Component;

class SemillaCreate extends Component
{
    public $breadcrumb;
    public $tiposSemilla;

    public $nombre;
    public $marca='';
    public $publicado = 0;
    public $tipo_semilla_id = 0;
    public $thc_porcentaje;
    public $caracteristicas;
    public $marcas;

    public $rules = [
        'nombre' => 'required',
        'marca' => 'required',
        'publicado' => 'required|boolean',
        'tipo_semilla_id' => 'required|exists:tipo_semillas,id',
        'thc_porcentaje' => 'required|numeric',
        'caracteristicas' => 'required',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('semillas') => 'Semillas',
            '' => 'Crear semilla',
        ];
        $this->tiposSemilla = TipoSemilla::select('id', 'nombre')->get();
        $this->marcas = Marca::where('marcable_type', Semilla::class)->select('id', 'nombre')->get();
    }

    public function render()
    {
        return view('livewire.admin.semillas.semilla-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('semillas');
    }

    public function store()
    {
        $data = $this->validate();
        $data['marca_id'] = $data['marca'];
        Semilla::create($data);

        session()->flash('success', 'Semilla creada correctamente');
        return redirect()->route('semillas');
    }
}
