<?php

namespace App\Http\Livewire\Admin\Semillas;

use Livewire\Component;

class Semillas extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Semillas'];

        return view('livewire.admin.semillas.semillas', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
