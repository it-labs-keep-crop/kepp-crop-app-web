<?php

namespace App\Http\Livewire\Admin\Semillas;

use App\Marca;
use App\Semilla;
use App\TipoSemilla;
use Livewire\Component;

class SemillaDetail extends Component
{
    public $semilla;
    public $breadcrumb;
    public $tiposSemilla;

    public $nombre;
    public $marca;
    public $publicado;
    public $tipo_semilla_id;
    public $thc_porcentaje;
    public $caracteristicas;
    public $marcas;

    public $rules = [
        'nombre' => 'required',
        'marca' => 'required',
        'publicado' => 'required|boolean',
        'tipo_semilla_id' => 'required|exists:tipo_semillas,id',
        'thc_porcentaje' => 'required|numeric|max:100|min:0',
        'caracteristicas' => 'required',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(Semilla $semilla)
    {
        $this->breadcrumb = [
            route('semillas') => 'Semillas',
            '' => 'Detalle',
        ];
        $this->semilla = $semilla;
        $this->tiposSemilla = TipoSemilla::select('id', 'nombre')->get();
        $this->marcas = Marca::where('marcable_type', Semilla::class)->select('id', 'nombre')->get();

        $this->fillWithModel();
    }

    public function render()
    {
        return view('livewire.admin.semillas.semilla-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();
        $data['marca_id'] = $data['marca'];
        $this->semilla->fill($data);

        $this->semilla->save();

        session()->flash('success', 'Semilla actualizada correctamente');
        return redirect()->route('semillas.detail', ['semilla' => $this->semilla->id]);
    }

    public function destroy()
    {
        $this->semilla->delete();
        session()->flash('success', 'Semilla eliminada correctamente');

        return redirect()->route('semillas');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'nombre' => $this->semilla->nombre,
            'publicado' => $this->semilla->publicado ? '1' : '0',
            'marca' => $this->semilla->marca_id,
            'tipo_semilla_id' => $this->semilla->tipo_semilla_id,
            'thc_porcentaje' => $this->semilla->thc_porcentaje,
            'caracteristicas' => $this->semilla->caracteristicas,
        ]);
    }
}
