<?php

namespace App\Http\Livewire\Admin\TipoSemillas;

use App\TipoSemilla;
use Livewire\Component;

class TipoSemillaDetail extends Component
{
  public $tipo;
  public $breadcrumb;

  public $nombre;
  public $publicado;
  public $ciclo_vida;
  public $tiempo_curado_min;
  public $tiempo_curado_max;
  public $temperatura_curado_recomendado;
  public $dias_inicio = [];

  public $rules = [
    'nombre' => 'required',
    'publicado' => 'required|boolean',
    'ciclo_vida' => 'required|numeric',
    'tiempo_curado_min' => 'numeric',
    'tiempo_curado_max' => 'numeric',
    'temperatura_curado_recomendado' => 'numeric',
  ];

  public $listeners = [
    'accept-modal' => 'destroy',
  ];

  public function mount(TipoSemilla $tipoSemilla)
  {
    $this->breadcrumb = [
      route('tiposemillas') => 'Tipo de Semillas',
      '' => 'Detalle',
    ];
    $this->tipo = $tipoSemilla;

    $this->fillWithModel();
  }

  public function render()
  {
    return view('livewire.admin.tipo-semillas.tipo-semilla-detail')
      ->layout('admin-dashboard');
  }

  public function updated($field)
  {
    $this->validateOnly($field);
  }

  public function cancel()
  {
    $this->fillWithModel();
    $this->dispatchBrowserEvent('stop-edit');
  }

  public function store()
  {
    $data = $this->validate();
    $this->tipo->fill($data);

    $this->tipo->save();

    session()->flash('success', 'Tipo de semilla actualizado correctamente');
    return redirect()->route('tiposemillas.detail', ['tipoSemilla' => $this->tipo->id]);
  }

  public function destroy()
  {
    $this->tipo->delete();
    session()->flash('success', 'Tipo de semilla eliminado correctamente');

    return redirect()->route('tiposemillas');
  }

  protected function fillWithModel()
  {
    $this->fill([
      'nombre' => $this->tipo->nombre,
      'publicado' => $this->tipo->publicado ? '1' : '0',
      'ciclo_vida' => $this->tipo->ciclo_vida,
      'tiempo_curado_min' => $this->tipo->tiempo_curado_min,
      'tiempo_curado_max' => $this->tipo->tiempo_curado_max,
      'temperatura_curado_recomendado' => $this->tipo->temperatura_curado_recomendado,
    ]);

    $this->tipo->etapas->each(function ($etapa) {
      $pivots = [
        'dia_inicio_pre_interior' => $etapa->pivot->dia_inicio_pre_interior,
        'dia_inicio_etapa_interior' => $etapa->pivot->dia_inicio_etapa_interior,
        'dia_inicio_pre_exterior' => $etapa->pivot->dia_inicio_pre_exterior,
        'dia_inicio_etapa_exterior' => $etapa->pivot->dia_inicio_etapa_exterior,
      ];

      $this->dias_inicio[$etapa->nombre] = $pivots;
    });
  }
}
