<?php

namespace App\Http\Livewire\Admin\TipoSemillas;

use Livewire\Component;

class TipoSemillas extends Component
{
  public function render()
  {
    $breadcrumb = ['' => 'Tipos de Semillas'];

    return view('livewire.admin.tipo-semillas.tipo-semillas', compact('breadcrumb'))
      ->layout('admin-dashboard');
  }
}
