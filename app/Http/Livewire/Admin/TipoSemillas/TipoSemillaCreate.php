<?php

namespace App\Http\Livewire\Admin\TipoSemillas;

use App\Etapa;
use App\TipoSemilla;
use Livewire\Component;

class TipoSemillaCreate extends Component
{
  public $title;
  public $nombre;
  public $publicado = 0;
  public $ciclo_vida;
  public $tiempo_curado_min;
  public $tiempo_curado_max;
  public $temperatura_curado_recomendado;

  public $etapas;
  public $dias_inicio = [];

  public function mount()
  {
    $this->etapas = Etapa::all();
    $this->etapas->each(function ($etapa) {
      $pivots = [
        'dia_inicio_pre_interior' => null,
        'dia_inicio_etapa_interior' => null,
        'dia_inicio_pre_exterior' => null,
        'dia_inicio_etapa_exterior' => null,
      ];

      $this->dias_inicio[$etapa->nombre] = $pivots;
    });
  }

  public function cancel()
  {
    return redirect()->route('tiposemillas');
  }

  public function storeTipoSemilla()
  {
    $data = $this->validate([
      'nombre' => 'required',
      'publicado' => 'required',
      'ciclo_vida' => 'required',
      'temperatura_curado_recomendado' => 'nullable|numeric',
      'tiempo_curado_min' => 'nullable|numeric',
      'tiempo_curado_max' => 'nullable|numeric',
    ]);

    $tipo = TipoSemilla::create($data);

    $this->etapas->each(function ($etapa) use ($tipo) {
      $tipo->etapas()->attach($etapa->id, $this->dias_inicio[$etapa->nombre]);
    });


    session()->flash('success', 'Tipo de semilla creado correctamente');
    return redirect()->route('tiposemillas');
  }

  public function render()
  {
    $this->title = 'Crear Tipo de semilla';
    return view('livewire.admin.tipo-semillas.tipo-semilla-create')
      ->layout('admin-dashboard');
  }
}
