<?php

namespace App\Http\Livewire\Admin\Etapas;

use App\Etapa;
use Livewire\Component;
use Livewire\WithFileUploads;

class EtapaDetail extends Component
{
    use WithFileUploads;

    public $etapa;
    public $breadcrumb;

    public $nombre;
    public $icon;
    public $descripcion;

    public $rules = [
        'nombre' => 'required',
        'descripcion' => 'required|max:254',
        'icon' => 'nullable|sometimes|image',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(Etapa $etapa)
    {
        $this->breadcrumb = [
            route('etapas') => 'Etapas',
            '' => 'Detalle',
        ];
        $this->etapa = $etapa;
        $this->fillWithModel();
    }

    public function render()
    {
        return view('livewire.admin.etapas.etapa-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();
        if ($this->icon) {
            $data['icon'] = str_replace('public/', 'storage/', $this->icon->store('public/etapas'));
        } else {
            unset($data['icon']);
        }

        $this->etapa->fill($data)->save();

        session()->flash('success', 'Etapa actualizada correctamente');
        return redirect()->route('etapas.detail', ['etapa' => $this->etapa->id]);
    }

    public function destroy()
    {
        $this->etapa->delete();
        session()->flash('success', 'Etapa eliminada correctamente');

        return redirect()->route('etapas');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'nombre' => $this->etapa->nombre,
            'descripcion' => $this->etapa->descripcion,
        ]);
    }
}
