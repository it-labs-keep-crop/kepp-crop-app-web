<?php

namespace App\Http\Livewire\Admin\Etapas;

use Livewire\Component;

class Etapas extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Etapas'];

        return view('livewire.admin.etapas.etapas', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
