<?php

namespace App\Http\Livewire\Admin\Etapas;

use App\Etapa;
use Livewire\Component;
use Livewire\WithFileUploads;

class EtapaCreate extends Component
{
    use WithFileUploads;

    public $breadcrumb;

    public $nombre;
    public $icon;
    public $descripcion;

    public $rules = [
        'nombre' => 'required',
        'descripcion' => 'required|max:254',
        'icon' => 'nullable|sometimes|image',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('etapas') => 'Etapas',
            '' => 'Crear etapa',
        ];
    }

    public function render()
    {
        return view('livewire.admin.etapas.etapa-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('etapas');
    }

    public function store()
    {
        $data = $this->validate();

        $data['icon'] = $this->icon ? str_replace('public/', 'storage/', $this->icon->store('public/etapas')) : null;

        Etapa::create($data);

        session()->flash('success', 'Etapa creada correctamente');
        return redirect()->route('etapas');
    }
}
