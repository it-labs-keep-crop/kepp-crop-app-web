<?php

namespace App\Http\Livewire\Admin\Fertilizantes;

use App\Etapa;
use App\Marca;
use App\Fertilizante;
use Livewire\Component;
use Livewire\WithFileUploads;

class FertilizanteCreate extends Component
{
    use WithFileUploads;

    public $breadcrumb;

    public $nombre;
    public $npk;
    public $marca = '';
    public $caracteristicas = '';
    public $formato;
    public $unidad_medida = '';
    public $imagen;
    public $publicado = '0';
    public $disolucion_litro;
    public $categoria = '';
    public $etapas;
    public $marcas;

    public $rules = [
        'nombre' => 'required',
        'caracteristicas' => 'required',
        'marca' => 'required',
        'npk' => 'required',
        'formato' => 'required|numeric',
        'unidad_medida' => 'required',
        'imagen' => 'nullable|sometimes|image',
        'publicado' => 'required|boolean',
        'disolucion_litro' => 'required|numeric|min:0',
        'categoria' => 'required',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('fertilizantes') => 'Fertilizantes',
            '' => 'Crear fertilizante',
        ];

        $this->etapas = Etapa::select('id', 'nombre')->get();
        $this->marcas = Marca::where('marcable_type', Fertilizante::class)->select('id', 'nombre')->get();
    }

    public function updated($field)
    {
      $this->validateOnly($field, [
        'npk' => ['required', 'string', function ($attribute, $value, $fail) {
          $npk = explode('-', $value);
          if (count($npk) == 3) {
            if (is_numeric($npk[0]) && is_numeric($npk[1]) && is_numeric($npk[2])) {
              return;
            }
          }
          return $fail('El formato para npk debe ser n-p-k');
        }],
      ]);
    }

    public function render()
    {
        return view('livewire.admin.fertilizantes.fertilizante-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('fertilizantes');
    }

    public function store()
    {
        $data = $this->validate();

        $data['marca_id'] = $data['marca'];
        $data['imagen'] = $this->imagen ? str_replace('public/', 'storage/', $this->imagen->store('public/fertilizantes')) : null;

        Fertilizante::create($data);

        session()->flash('success', 'Fertilizante creado correctamente');
        return redirect()->route('fertilizantes');
    }
}
