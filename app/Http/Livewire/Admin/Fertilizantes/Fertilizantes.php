<?php

namespace App\Http\Livewire\Admin\Fertilizantes;

use Livewire\Component;

class Fertilizantes extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Fertilizantes'];

        return view('livewire.admin.fertilizantes.fertilizantes', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
