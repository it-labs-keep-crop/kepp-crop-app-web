<?php

namespace App\Http\Livewire\Admin\Fertilizantes;

use App\Etapa;
use App\Marca;
use App\Fertilizante;
use Livewire\Component;
use Livewire\WithFileUploads;

class FertilizanteDetail extends Component
{
    use WithFileUploads;

    public $fertilizante;
    public $breadcrumb;

    public $nombre;
    public $marca = '';
    public $npk;
    public $caracteristicas;
    public $formato;
    public $unidad_medida;
    public $publicado;
    public $imagen;
    public $disolucion_litro;
    public $categoria;
    public $etapas;
    public $marcas;

    public $rules = [
        'nombre' => 'required',
        'marca' => 'required',
        'caracteristicas' => 'required',
        'npk' => 'required',
        'formato' => 'required|numeric',
        'unidad_medida' => 'required',
        'imagen' => 'nullable|sometimes|image',
        'publicado' => 'required|boolean',
        'disolucion_litro' => 'required|numeric|min:0',
        'categoria' => 'required',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(Fertilizante $fertilizante)
    {
        $this->breadcrumb = [
            route('fertilizantes') => 'Fertilizantes',
            '' => 'Detalle',
        ];
        $this->fertilizante = $fertilizante;
        $this->etapas = Etapa::select('id', 'nombre')->get();
        $this->marcas = Marca::where('marcable_type', Fertilizante::class)->select('id', 'nombre')->get();
        $this->fillWithModel();
    }

    public function render()
    {
        return view('livewire.admin.fertilizantes.fertilizante-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
      $this->validateOnly($field, [
        'npk' => ['required', 'string', function ($attribute, $value, $fail) {
          $npk = explode('-', $value);
          if (count($npk) == 3) {
            if (is_numeric($npk[0]) && is_numeric($npk[1]) && is_numeric($npk[2])) {
              return;
            }
          }
          return $fail('El formato para npk debe ser n-p-k');
        }],
      ]);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();
        if ($this->imagen) {
            $data['imagen'] = str_replace('public/', 'storage/', $this->imagen->store('public/fertilizantes'));
        } else {
            unset($data['imagen']);
        }
        $data['marca_id'] = $data['marca'];
        $this->fertilizante->fill($data)->save();

        session()->flash('success', 'Fertilizante editado correctamente');
        return redirect()->route('fertilizantes.detail', ['fertilizante' => $this->fertilizante->id]);
    }

    public function destroy()
    {
        $this->fertilizante->delete();
        session()->flash('success', 'Fertilizante eliminado correctamente');

        return redirect()->route('fertilizantes');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'nombre' => $this->fertilizante->nombre,
            'marca' => $this->fertilizante->marca_id,
            'npk' => $this->fertilizante->npk,
            'caracteristicas' => $this->fertilizante->caracteristicas,
            'formato' => $this->fertilizante->formato,
            'unidad_medida' => $this->fertilizante->unidad_medida,
            'publicado' => $this->fertilizante->publicado ? '1' : '0',
            'disolucion_litro' => $this->fertilizante->disolucion_litro,
            'categoria' => $this->fertilizante->categoria,
        ]);
    }
}
