<?php

namespace App\Http\Livewire\Admin\Marcas;

use App\Marca;
use App\Semilla;
use App\Fertilizante;
use Livewire\Component;

class MarcaCreate extends Component
{
    public $breadcrumb;
    public $nombre;
    public $tipo='';
    public $tipos;

    public $rules = [
        'nombre' => 'required|max:254',
        'tipo' => 'required',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('marcas') => 'Marcas',
            '' => 'Crear marca',
        ];

        $this->tipos = [
          Semilla ::class => 'Semilla',
          Fertilizante::class => 'Fertilizante',
        ];
    }

    public function render()
    {
        return view('livewire.admin.marcas.marca-create')
          ->layout('admin-dashboard');
    }

        public function cancel()
    {
        return redirect()->route('marcas');
    }

    public function store()
    {
        $data = $this->validate();
        $data['marcable_type'] = $data['tipo'];
        Marca::create($data);

        session()->flash('success', 'Marca creada correctamente');
        return redirect()->route('marcas');
    }
}
