<?php

namespace App\Http\Livewire\Admin\Marcas;

use App\Marca;
use App\Semilla;
use App\Fertilizante;
use Livewire\Component;

class MarcaDetail extends Component
{
    public $marca;
    public $breadcrumb;
    public $tipos;

    public $nombre;
    public $tipo;

    public $rules = [
        'nombre' => 'required|max:254',
        'tipo' => 'required',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(Marca $marca)
    {
        $this->breadcrumb = [
            route('marcas') => 'Marcas',
            '' => 'Marca',
        ];
        $this->marca = $marca;
        $this->fillWithModel();

        $this->tipos = [
          Semilla::class => 'Semilla',
          Fertilizante::class => 'Fertilizante',
        ];
    }

    public function render()
    {
        return view('livewire.admin.marcas.marca-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();

        $data['marcable_type'] = $data['tipo'];

        $this->marca->fill($data)->save();

        session()->flash('success', 'Marca actualizada correctamente');
        return redirect()->route('marcas.detail', ['marca' => $this->marca->id]);
    }

    public function destroy()
    {
        $this->marca->delete();
        session()->flash('success', 'Marca eliminada correctamente');

        return redirect()->route('marcas');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'nombre' => $this->marca->nombre,
            'tipo' => $this->marca->marcable_type,
        ]);
    }
}
