<?php

namespace App\Http\Livewire\Admin\Marcas;

use Livewire\Component;

class Marcas extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Marcas'];

        return view('livewire.admin.marcas.marcas', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
