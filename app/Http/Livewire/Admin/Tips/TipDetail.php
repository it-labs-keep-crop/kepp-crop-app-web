<?php

namespace App\Http\Livewire\Admin\Tips;

use App\Tip;
use App\Etapa;
use App\Maceta;
use App\TipoSemilla;
use App\Fertilizante;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class TipDetail extends Component
{
    use WithFileUploads;

    public $tip;
    public $breadcrumb;
    public $tipos;
    public $tipos_label;

    public $titulo;
    public $contenido;
    public $imagen;
    public $listado;
    public $link;
    public $tipable_id;
    public $tipable_type;

    public $rules = [
        'titulo' => 'required|max:254',
        'contenido' => 'required|max:254',
        'listado' => 'required',
        'link' => 'sometimes',
        'imagen' => 'nullable|sometimes|image',
        'tipable_type' => 'nullable',
        'tipable_id' => 'nullable',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(Tip $tip)
    {
        $this->breadcrumb = [
            route('tips') => 'Tips',
            '' => 'Detalle',
        ];
        $this->tip = $tip;
        $this->fillWithModel();

        $this->tipos_label = [
          Etapa::class => 'Etapa',
          Maceta::class => 'Maceta',
          TipoSemilla::class => 'Tipo de Semilla',
          Fertilizante::class => 'Fertilizante',
        ];

        $this->tipos = [
          Etapa::class => Etapa::select('id', 'nombre')->get(),
          Maceta::class => Maceta::select('id', 'nombre')->get(),
          TipoSemilla::class => TipoSemilla::select('id', 'nombre')->get(),
          Fertilizante::class => Fertilizante::select('id', 'nombre')->get(),
        ];
    }

    public function render()
    {
        return view('livewire.admin.tips.tip-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();

        if ($this->imagen) {
            $data['imagen'] = str_replace('public/', 'storage/', $this->imagen->store('public/tips'));
        } else {
            unset($data['imagen']);
        }

        $this->tip->fill($data)->save();

        session()->flash('success', 'Tip actualizado correctamente');
        return redirect()->route('tips.detail', ['tip' => $this->tip->id]);
    }

    public function destroy()
    {
        $this->tipo->delete();
        session()->flash('success', 'Tip eliminado correctamente');

        return redirect()->route('tips');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'titulo' => $this->tip->titulo,
            'contenido' => $this->tip->contenido,
            'listado' => $this->tip->listado,
            'link' => $this->tip->link,
            'tipable_type' => $this->tip->tipable_type,
            'tipable_id' => $this->tip->tipable_id,
        ]);
    }
}
