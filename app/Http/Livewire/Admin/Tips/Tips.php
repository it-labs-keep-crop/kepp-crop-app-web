<?php

namespace App\Http\Livewire\Admin\Tips;

use Livewire\Component;

class Tips extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Tips'];

        return view('livewire.admin.tips.tips', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
