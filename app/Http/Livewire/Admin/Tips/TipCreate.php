<?php

namespace App\Http\Livewire\Admin\Tips;

use App\Tip;
use App\Etapa;
use App\Maceta;
use App\TipoSemilla;
use App\Fertilizante;
use Livewire\Component;
use Livewire\WithFileUploads;

class TipCreate extends Component
{
    use WithFileUploads;

    public $breadcrumb;
    public $tipos;
    public $tipos_label;

    public $titulo;
    public $contenido;
    public $link;
    public $imagen;
    public $listado = 1;
    public $tipable_type = "0";
    public $tipable_id;

    public $rules = [
        'titulo' => 'required',
        'contenido' => 'required|max:254',
        'listado' => 'required',
        'tipable_type' => 'nullable',
        'tipable_id' => 'nullable',
        'imagen' => 'nullable|sometimes|image',
        'link' => 'sometimes',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('tips') => 'Tips',
            '' => 'Crear tip',
        ];

        $this->tipos_label = [
          Etapa::class => 'Etapa',
          Maceta::class => 'Maceta',
          TipoSemilla::class => 'Tipo de Semilla',
          Fertilizante::class => 'Fertilizante',
        ];

        $this->tipos = [
          Etapa::class => Etapa::select('id', 'nombre')->get(),
          Maceta::class => Maceta::select('id', 'nombre')->get(),
          TipoSemilla::class => TipoSemilla::select('id', 'nombre')->get(),
          Fertilizante::class => Fertilizante::select('id', 'nombre')->get(),
        ];
    }

    public function render()
    {
        return view('livewire.admin.tips.tip-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('tips');
    }

    public function store()
    {
        $data = $this->validate();

        if ($data['tipable_type'] == "0" || $data['tipable_type'] == null) {
          $data['tipable_type'] = null;
          $data['tipable_id'] = null;
        }

        $data['imagen'] = $this->imagen ? str_replace('public/', 'storage/', $this->imagen->store('public/tips')) : null;

        Tip::create($data);

        session()->flash('success', 'Tip creado correctamente');
        return redirect()->route('tips');
    }
}
