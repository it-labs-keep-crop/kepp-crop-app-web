<?php

namespace App\Http\Livewire\Admin\TipoNotificaciones;

use Livewire\Component;

class TipoNotificaciones extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Tipos de Notificacion'];

        return view('livewire.admin.tipo-notificaciones.tipo-notificaciones', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
