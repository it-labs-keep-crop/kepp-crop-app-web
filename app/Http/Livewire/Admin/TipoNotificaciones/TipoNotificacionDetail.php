<?php

namespace App\Http\Livewire\Admin\TipoNotificaciones;

use Livewire\Component;
use App\TipoNotificacion;
use Livewire\WithFileUploads;

class TipoNotificacionDetail extends Component
{
    use WithFileUploads;

    public $tipo;
    public $breadcrumb;

    public $icon;
    public $nombre;
    public $contenido;

    public $rules = [
        'nombre' => 'required|max:254',
        'contenido' => 'required|max:254',
        'icon' => 'nullable|sometimes|image',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(TipoNotificacion $tipo)
    {
        $this->breadcrumb = [
            route('tipoNotificaciones') => 'Tipo de Notificacion',
            '' => 'Detalle',
        ];
        $this->tipo = $tipo;
        $this->fillWithModel();
    }

    public function render()
    {
        return view('livewire.admin.tipo-notificaciones.tipo-notificacion-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();
        if ($this->icon) {
            $data['icon'] = str_replace('public/', 'storage/', $this->icon->store('public/notificaciones'));
        } else {
            unset($data['icon']);
        }

        $this->tipo->fill($data)->save();

        session()->flash('success', 'Tipo de Notificación actualizado correctamente');
        return redirect()->route('tipoNotificaciones.detail', ['tipo' => $this->tipo->id]);
    }

    public function destroy()
    {
        $this->tipo->delete;
        session()->flash('success', 'Tipo de Notificación eliminada correctamente');

        return redirect()->route('tipoNotificaciones');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'nombre' => $this->tipo->nombre,
            'contenido' => $this->tipo->contenido,
        ]);
    }
}
