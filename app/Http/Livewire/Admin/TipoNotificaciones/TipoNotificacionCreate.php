<?php

namespace App\Http\Livewire\Admin\TipoNotificaciones;

use Livewire\Component;

class TipoNotificacionCreate extends Component
{
    public function render()
    {
        return view('livewire.admin.tipo-notificaciones.tipo-notificacion-create');
    }
}
