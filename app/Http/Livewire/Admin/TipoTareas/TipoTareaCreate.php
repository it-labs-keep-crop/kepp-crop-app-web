<?php

namespace App\Http\Livewire\Admin\TipoTareas;

use App\TipoTarea;
use Livewire\Component;

class TipoTareaCreate extends Component
{
    public $titulo;
    public $template;
    public $variables;
    public $breadcrumb;

    public $rules = [
        'titulo' => 'required',
        'template' => 'required|max:254',
        'variables' => 'required',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('tipotareas') => 'Tipos de Tarea',
            '' => 'Crear tipo de tarea',
        ];
    }

    public function render()
    {
        return view('livewire.admin.tipo-tareas.tipo-tarea-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('tipotareas');
    }

    public function store()
    {
        $data = $this->validate();

        TipoTarea::create($data);

        session()->flash('success', 'Tipo de tarea creado correctamente');
        return redirect()->route('tipotareas');
    }
}
