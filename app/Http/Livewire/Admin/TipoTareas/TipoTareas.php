<?php

namespace App\Http\Livewire\Admin\TipoTareas;

use Livewire\Component;

class TipoTareas extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Tipos de Tareas'];

        return view('livewire.admin.tipo-tareas.tipo-tareas', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
