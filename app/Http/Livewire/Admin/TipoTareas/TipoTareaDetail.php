<?php

namespace App\Http\Livewire\Admin\TipoTareas;

use App\TipoTarea;
use Livewire\Component;

class TipoTareaDetail extends Component
{
    public $tipo;
    public $breadcrumb;

    public $titulo;
    public $horario_min;
    public $horario_max;
    public $template;
    public $variables;

    public $rules = [
        'titulo' => 'required',
        'horario_min' => 'required',
        'horario_max' => 'required',
        'template' => 'required|max:254',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(TipoTarea $tipoTarea)
    {
        $this->breadcrumb = [
            route('tipotareas') => 'Tipos de Tareas',
            '' => 'Detalle',
        ];
        $this->tipo = $tipoTarea;
        $this->fillWithModel();
    }

    public function render()
    {
        return view('livewire.admin.tipo-tareas.tipo-tarea-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();
        $this->tipo->fill($data)->save();

        session()->flash('success', 'Tipo de tarea actualizado correctamente');
        return redirect()->route('tipotareas.detail', ['tipoTarea' => $this->tipo->id]);
    }

    public function destroy()
    {
        $this->tipo->delete();
        session()->flash('success', 'Tipo de tarea eliminado correctamente');

        return redirect()->route('tipotareas');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'titulo' => $this->tipo->titulo,
            'horario_min' => $this->tipo->horario_min,
            'horario_max' => $this->tipo->horario_max,
            'template' => $this->tipo->template,
            'variables' => $this->tipo->variables,
        ]);
    }
}
