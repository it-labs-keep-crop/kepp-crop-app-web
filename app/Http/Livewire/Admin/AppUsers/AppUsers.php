<?php

namespace App\Http\Livewire\Admin\AppUsers;

use App\User;
use Livewire\Component;

class AppUsers extends Component
{
    public function render()
    {
        $breadcrumb = ['' => 'Usuarios'];

        return view('livewire.admin.app-users.app-users', compact('breadcrumb'))
          ->layout('admin-dashboard');
    }
}
