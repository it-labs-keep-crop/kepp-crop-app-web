<?php

namespace App\Http\Livewire\Admin\AppUsers;

use App\User;
use Livewire\Component;
use Livewire\WithFileUploads;

class AppUserDetail extends Component
{
    use WithFileUploads;

    public $user;
    public $breadcrumb;

    public $name;
    public $email;
    public $phone;
    public $avatar;

    public $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'avatar' => 'nullable|sometimes|image',
    ];

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public function mount(User $user)
    {
        $this->breadcrumb = [
            route('app-users') => 'Usuarios',
            '' => 'Detalle',
        ];
        $this->user = $user;
        $this->fillWithModel();
    }

    public function render()
    {
        return view('livewire.admin.app-users.app-user-detail')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        $this->fillWithModel();
        $this->dispatchBrowserEvent('stop-edit');
    }

    public function store()
    {
        $data = $this->validate();
        if ($this->avatar) {
            $data['avatar'] = str_replace('public/', 'storage/', $this->avatar->store('public/avatars'));
        } else {
            unset($data['avatar']);
        }
        $this->user->fill($data)->save();

        session()->flash('success', 'Usuario editado correctamente');

        return redirect()->route('app-users.detail', ['user' => $this->user->id]);
    }

    public function destroy()
    {
        $this->user->delete();
        session()->flash('success', 'Usuario eliminado correctamente');

        return redirect()->route('app-users');
    }

    protected function fillWithModel()
    {
        $this->fill([
            'name' => $this->user->name,
            'email' => $this->user->email,
            'phone' => $this->user->phone,
        ]);
    }
}
