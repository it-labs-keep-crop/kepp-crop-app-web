<?php

namespace App\Http\Livewire\Admin\AppUsers;

use App\User;
use Livewire\Component;
use Livewire\WithFileUploads;

class AppUserCreate extends Component
{
    use WithFileUploads;

    public $breadcrumb;

    public $name;
    public $email;
    public $phone;
    public $avatar;
    public $password;
    public $password_confirmation;

    public $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'password' => 'required|same:password_confirmation|min:6',
        'avatar' => 'nullable|image',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('app-users') => 'Usuarios',
            '' => 'Crear usuario',
        ];
    }

    public function render()
    {
        return view('livewire.admin.app-users.app-user-create')
          ->layout('admin-dashboard');
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function cancel()
    {
        return redirect()->route('app-users');
    }

    public function store()
    {
        $this->validate();

        $path = $this->avatar ? str_replace('public/', 'storage/', $this->avatar->store('public/avatars')) : null;

        User::create([
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'password' => bcrypt($this->password),
            'avatar' => $path,
        ]);

        session()->flash('success', 'Usuario creado correctamente');

        return redirect()->route('app-users');
    }
}
