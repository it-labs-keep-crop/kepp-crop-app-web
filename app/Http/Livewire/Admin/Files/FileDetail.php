<?php

namespace App\Http\Livewire\Admin\Files;

use Livewire\Component;

class FileDetail extends Component
{
    public function render()
    {
        return view('livewire.admin.files.file-detail');
    }
}
