<?php

namespace App\Http\Livewire\Admin\Files;

use App\File;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;


class Files extends Component
{
    use WithPagination;

    public $q;
    public $selectedId;

    public $listeners = [
        'accept-modal' => 'destroy',
    ];

    public $mapExtensionIcon = [
      'pdf' => 'far-file-pdf',
      'xls' => 'far-file-excel',
      'xlsx' => 'far-file-excel',
      'xlsm' => 'far-file-excel',
      'xml' => 'far-file-excel',
      'csv' => 'far-file-excel',
      'png' => 'far-file-image',
      'jpg' => 'far-file-image',
      'jpeg' => 'far-file-image',
      'doc' => 'far-file-word',
      'docx' => 'far-file-word',
      'zip' => 'far-file-archive',
      'rar' => 'far-file-archive',
      'tar' => 'far-file-archive',
    ];

    public $mapExtensionClass = [
      'pdf' => 'w-6 h-6 mr-4 text-red-400',
      'xls' => 'w-6 h-6 mr-4 text-green-400',
      'xlsx' => 'w-6 h-6 mr-4 text-green-400',
      'xlsm' => 'w-6 h-6 mr-4 text-green-400',
      'xml' => 'w-6 h-6 mr-4 text-green-400',
      'png' => 'w-6 h-6 mr-4 text-purple-400',
      'jpg' => 'w-6 h-6 mr-4 text-purple-400',
      'jpeg' => 'w-6 h-6 mr-4 text-purple-400',
      'doc' => 'w-6 h-6 mr-4 text-blue-400',
      'docx' => 'w-6 h-6 mr-4 text-blue-400',
    ];

    protected $queryString = [
        'q' => ['except' => ''],
    ];

    public function mount()
    {
        $this->q = request()->q ?? '';
    }

    public function getIcon($extension)
    {
        if (array_key_exists($extension, $this->mapExtensionIcon)) {
          return $this->mapExtensionIcon[$extension];
        }
        return 'far-file';
    }

    public function getClass($extension)
    {
        if (array_key_exists($extension, $this->mapExtensionClass)) {
          return $this->mapExtensionClass[$extension];
        }
        return 'w-6 h-6 mr-4 text-gray-400';
    }

    public function download($fileId)
    {
        $file = File::find($fileId);
        if ($file->path) {
          return Storage::download(str_replace('storage/', 'public/', $file->path), $file->name .'.'. $file->extension);
        }
        session()->flash('danger', 'Archivo no encontrado');
        return redirect()->route('files');
    }

    public function handleDestroy($id)
    {
        $this->selectedId = $id;
        $this->emit('open-modal');
    }

    public function destroy()
    {
        $file = File::find($this->selectedId);
        Storage::delete(str_replace('storage/', 'public/', $file->path));
        $file->delete();
        session()->flash('success', 'Archivo eliminado correctamente');

        return redirect()->route('files');
    }

    public function render()
    {
        $files = File::where('name', 'ilike', '%'.$this->q.'%')->orderBy('name')->paginate(10);
        $breadcrumb = ['' => 'Archivos'];

        return view('livewire.admin.files.files', compact('files', 'breadcrumb'))
          ->layout('admin-dashboard');
    }
}
