<?php

namespace App\Http\Livewire\Admin\Files;

use App\File;
use Livewire\Component;
use Livewire\WithFileUploads;

class FileCreate extends Component
{
    use WithFileUploads;

    public $breadcrumb;

    public $name;
    public $file;

    public $rules = [
        'name' => 'required',
        'file' => 'nullable|sometimes|file',
    ];

    public function mount()
    {
        $this->breadcrumb = [
            route('files') => 'Archivos',
            '' => 'Crear archivo',
        ];
    }

    public function render()
    {
        return view('livewire.admin.files.file-create')
          ->layout('admin-dashboard');
    }

    public function cancel()
    {
        return redirect()->route('files');
    }

    public function store()
    {
        $data = $this->validate();

        $data['path'] = $this->file ? str_replace('public/', 'storage/', $this->file->store('public/files')) : null;
        $extension = explode('.', $data['path']);

        File::create([
          'name' => $data['name'],
          'path' => $data['path'],
          'extension' => end($extension),
        ]);

        session()->flash('success', 'Archivo creado correctamente');
        return redirect()->route('files');
    }
}
