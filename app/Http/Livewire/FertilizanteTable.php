<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Fertilizante;

class FertilizanteTable extends DataTableComponent
{
    public string $searchLabel = 'Buscar';
    public bool $showSorting = false;
    public string $emptyMessage = 'No se encontraron elementos que coincidan con tu búsqueda.';

    public function columns(): array
    {
        return [
            Column::make('Nombre')
                ->sortable()
                ->searchable(),
            Column::make('Marca', 'marca.nombre')
                ->sortable(function(Builder $query, $direction) {
                  return $query
                    ->join('marcas', 'fertilizantes.marca_id', '=', 'marcas.id')
                    ->orderBy('marcas.nombre', $direction)
                    ->select('fertilizantes.*');
                })
                ->searchable(),
            Column::make('Formato')
                ->sortable(),
            Column::make('Unidad Medida', 'unidad_medida')
                ->sortable(),
            Column::make('Publicado')
                ->format(function($value) {
                  if ($value) {
                    return '<span class="inline-flex px-3 py-1 font-semibold text-green-800 bg-green-100 rounded-full text-s leading-5">
                      Publicado
                      </span>';
                  }
                  return '<span class="inline-flex px-3 py-1 font-semibold text-indigo-800 bg-indigo-200 rounded-full text-s leading-5">
                    Borrador
                    </span>';
                })
                ->asHtml(),
            Column::make('Disolución Litro', 'disolucion_litro')
                ->sortable(),
            Column::make('Categoría', 'categoria')
                ->sortable()
                ->searchable(),
            Column::make('', 'id')
                ->format(function($value) {
                    return '<a href="'. route('fertilizantes.detail', ['fertilizante' => $value]) .'" class="text-indigo-500 hover:text-indigo-700">Ver</a>';
                })
                ->asHtml(),
        ];
    }

    public function query(): Builder
    {
        return Fertilizante::query();
    }
}
