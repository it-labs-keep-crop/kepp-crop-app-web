<?php

namespace App\Http\Livewire;

use App\TipoNotificacion;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class TipoNotificacionTable extends DataTableComponent
{
    public string $searchLabel = 'Buscar';
    public bool $showSorting = false;
    public string $emptyMessage = 'No se encontraron elementos que coincidan con tu búsqueda.';

    public function columns(): array
    {
        return [
            Column::make('Nombre')
                ->sortable()
                ->searchable(),
            Column::make('', 'id')
                ->format(function($value) {
                    return '<a href="'. route('tipoNotificaciones.detail', ['tipo' => $value]) .'" class="text-indigo-500 hover:text-indigo-700">Ver</a>';
                })
                ->asHtml(),
        ];
    }

    public function query(): Builder
    {
        return TipoNotificacion::query();
    }
}
