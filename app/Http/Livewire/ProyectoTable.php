<?php

namespace App\Http\Livewire;

use App\Proyecto;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class ProyectoTable extends DataTableComponent
{
    public string $searchLabel = 'Buscar';
    public bool $showSorting = false;
    public string $emptyMessage = 'No se encontraron elementos que coincidan con tu búsqueda.';

    public function columns(): array
    {
        return [
            Column::make('Nombre')
                ->sortable()
                ->searchable(),
            Column::make('Tipo cultivo', 'tipo_cultivo')
                ->sortable()
                ->searchable(),
            Column::make('Tipo semilla', 'tipoSemilla.nombre')
                ->sortable(function(Builder $query, $direction) {
                    return $query
                        ->join('tipo_semillas', 'proyectos.tipo_semilla_id', '=', 'tipo_semillas.id')
                        ->orderBy('tipo_semillas.nombre', $direction)
                        ->select('proyectos.*');
                })
                ->searchable(),
            Column::make('Cultivos', 'cultivos_count')
                ->sortable(),
            Column::make('', 'id')
                ->format(function($value) {
                    return '<a href="'. route('proyectos.detail', ['proyecto' => $value]) .'" class="text-indigo-500 hover:text-indigo-700">Ver</a>';
                })
                ->asHtml(),
        ];
    }

    public function query(): Builder
    {
        return Proyecto::with('tipoSemilla')->withCount('cultivos');
    }
}
