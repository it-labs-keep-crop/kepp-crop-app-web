<?php

namespace App\Http\Livewire;

use App\Maceta;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class MacetaTable extends DataTableComponent
{
    public string $searchLabel = 'Buscar';
    public bool $showSorting = false;
    public string $emptyMessage = 'No se encontraron elementos que coincidan con tu búsqueda.';

    public function columns(): array
    {
        return [
            Column::make('Nombre')
                ->sortable()
                ->searchable(),
            Column::make('Capacidad (lt)', 'capacidad_litros')
                ->sortable(),
            Column::make('Agua')
                ->sortable(),
            Column::make('Dimensiones')
                ->sortable(),
            Column::make('', 'id')
                ->format(function($value) {
                    return '<a href="'. route('macetas.detail', ['maceta' => $value]) .'" class="text-indigo-500 hover:text-indigo-700">Ver</a>';
                })
                ->asHtml(),
        ];
    }

    public function query(): Builder
    {
        return Maceta::query();
    }
}
