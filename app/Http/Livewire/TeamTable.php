<?php

namespace App\Http\Livewire;

use App\AdminUser;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class TeamTable extends DataTableComponent
{
    public string $searchLabel = 'Buscar';
    public bool $showSorting = false;
    public string $emptyMessage = 'No se encontraron elementos que coincidan con tu búsqueda.';

    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name')
                ->sortable()
                ->searchable(),
            Column::make('Email')
                ->sortable()
                ->searchable(),
            Column::make('Avatar')
                ->format(function($value) {
                  if ($value) {
                    $asset = asset($value);
                    return '<img class="flex-shrink-0 w-12 h-12 rounded-full bg-gray" src="'. $asset .'" alt="">';
                  }
                    $asset = asset('img/user.png');
                  return '<img class="flex-shrink-0 w-12 h-12 rounded-full bg-gray" src="'. $asset .'" alt="">';
                })
                ->asHtml(),
            Column::make('', 'id')
                ->format(function($value) {
                    return '<a href="'. route('users.detail', ['adminUser' => $value]) .'" class="text-indigo-500 hover:text-indigo-700">Ver</a>';
                })
                ->asHtml(),
        ];
    }

    public function query(): Builder
    {
        return AdminUser::query();
    }
}
