<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;

class Login extends Component
{
  public $email = '';
  public $password = '';
  public $remember = false;

  protected $rules = [
    'email' => 'required|email',
    'password' => 'required',
  ];

  public function authenticate()
  {
    $this->validate();

    $credentials = [
      'email' => $this->email,
      'password' => $this->password
    ];

    if (!Auth::guard('admins')->attempt($credentials)) {
      $this->addError('email', trans('auth.failed'));

      return;
    }

    redirect()->route('dashboard');
  }

  public function render()
  {
    return view('livewire.auth.login');
  }
}
