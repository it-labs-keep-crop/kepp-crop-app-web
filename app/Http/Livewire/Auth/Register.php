<?php

namespace App\Http\Livewire\Auth;

use App\User;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class Register extends Component
{
    public $first_name = '';
    public $last_name = '';
    public $email = '';
    public $password = '';
    public $passwordConfirmation = '';
    public $address_location = '';
    public $address_text = '';
    public $phone_iso_code;
    public $phone_code;
    public $phone_number;
    public $isValidPhoneNumber = true;

    protected $listeners = ['onPickAddress', 'onChangePhone'];

    public function onChangePhone($phone)
    {
        $phone = json_decode($phone, true);
        $this->phone_code = $phone['code'] ?? null;
        $this->phone_iso_code = $phone['iso'] ?? null;
        $this->phone_number = str_replace(' ', '', $phone['number'] ?? '');
        $this->isValidPhoneNumber = $phone['isValid'];
        $this->checkValidationPhoneNumber();
    }

    public function onPickAddress($address)
    {
        $address = json_decode($address, true);
        $coordinates = $address['center'];
        $this->address_location = [$coordinates[1], $coordinates[0]];
        $this->address_text = $address['place_name'];
        $this->resetErrorBag('address_location');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'first_name'       => 'required',
            'last_name'        => 'required',
            'address_location' => 'required',
            'email'            => ['required', 'email', 'unique:users'],
            'password'         => ['required', 'min:8', 'same:passwordConfirmation'],
        ]);
    }

    public function register()
    {
        $this->validate([
            'first_name'         => 'required',
            'last_name'          => 'required',
            'address_location'   => 'required',
            'address_location'   => 'required',
            'isValidPhoneNumber' => 'required|boolean|in:1',
            'email'              => ['required', 'email', 'unique:users'],
            'password'           => ['required', 'min:8', 'same:passwordConfirmation'],
        ]);

        $user = User::create([
            'email'            => $this->email,
            'first_name'       => $this->first_name,
            'last_name'        => $this->last_name,
            'phone_iso_code'   => $this->phone_iso_code,
            'phone_code'       => $this->phone_code,
            'phone_number'     => $this->phone_number,
            'address_text'     => $this->address_text,
            'address_location' => $this->address_location,
            'password'         => Hash::make($this->password),
        ]);

        Auth::login($user, true);

        redirect(route('app'));
    }

    public function render()
    {
        return view('livewire.auth.register');
    }

    private function checkValidationPhoneNumber()
    {
        if ($this->isValidPhoneNumber) {
            $this->resetErrorBag('isValidPhoneNumber');
        } else {
            $this->addError('isValidPhoneNumber', "The selected is valid phone number is invalid.");
        }
    }
}
