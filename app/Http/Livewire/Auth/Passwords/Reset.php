<?php

namespace App\Http\Livewire\Auth\Passwords;

use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;

class Reset extends Component
{
  public $token;
  public $from;
  public $email;
  public $password;
  public $passwordConfirmation;

  public function mount($from, $token)
  {
    $this->from = $from;
    $this->token = $token;
  }

  public function resetPassword()
  {
    $this->validate([
      'token' => 'required',
      'email' => 'required|email',
      'password' => 'required|min:6|same:passwordConfirmation',
    ]);

    $response = $this->broker()->reset(
      [
        'token' => $this->token,
        'email' => $this->email,
        'password' => $this->password
      ],
      function ($user, $password) {
        $user->password = bcrypt($password);
        $user->setRememberToken(Str::random(60));
        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);
      }
    );

    \Log::info([
      'token' => $this->token,
      'response' => $response,
    ]);

    if ($response == Password::PASSWORD_RESET) {
      session()->flash(trans($response));

      return redirect(route('home'));
    }

    $this->addError('email', trans($response));
  }

  /**
   * Get the broker to be used during password reset.
   *
   * @return \Illuminate\Contracts\Auth\PasswordBroker
   */
  public function broker()
  {
    if ($this->from == 'mobile') {
      return Password::broker('users');
    }
    return Password::broker('admins');
  }

  /**
   * Get the guard to be used during password reset.
   *
   * @return \Illuminate\Contracts\Auth\StatefulGuard
   */
  protected function guard()
  {
    if ($this->from == 'mobile') {
      return Auth::guard('users');
    }
    return Auth::guard('admins');
  }

  public function render()
  {
    return view('livewire.auth.passwords.reset');
  }
}
