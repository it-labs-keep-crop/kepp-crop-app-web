<?php

namespace App\Http\Livewire\Auth\Passwords;

use Livewire\Component;
use Illuminate\Support\Facades\Password;

class Email extends Component
{
  public $email;
  public $resetFrom;
  public $emailSentMessage = false;

  public $rules = [
    'email' => 'required|email',
  ];

  public function mount($from)
  {
    $this->resetFrom = $from;
  }

  public function sendResetPasswordLink()
  {
    $response = $this->broker()->sendResetLink(['email' => $this->email]);

    if ($response == Password::RESET_LINK_SENT) {
      $this->emailSentMessage = trans($response);

      return;
    }

    $this->addError('email', trans($response));
  }

  /**
   * Get the broker to be used during password reset.
   *
   * @return \Illuminate\Contracts\Auth\PasswordBroker
   */
  public function broker()
  {
    if ($this->resetFrom == 'mobile') {
      return Password::broker('users');
    }
    return Password::broker('admins');
  }

  public function render()
  {
    return view('livewire.auth.passwords.email');
  }
}
