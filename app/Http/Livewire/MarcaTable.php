<?php

namespace App\Http\Livewire;

use App\Marca;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class MarcaTable extends DataTableComponent
{
    public string $searchLabel = 'Buscar';
    public bool $showSorting = false;
    public string $emptyMessage = 'No se encontraron elementos que coincidan con tu búsqueda.';

    public function columns(): array
    {
        return [
            Column::make('Nombre')
                ->sortable()
                ->searchable(),
            Column::make('Tipo', 'marcable_type')
                ->sortable()
                ->searchable()
                ->format(function($value) {
                    return explode('\\', $value)[1];
                }),
            Column::make('', 'id')
                ->format(function($value) {
                    return '<a href="'. route('marcas.detail', ['marca' => $value]) .'" class="text-indigo-500 hover:text-indigo-700">Ver</a>';
                })
                ->asHtml(),
        ];
    }

    public function query(): Builder
    {
        return Marca::query();
    }
}
