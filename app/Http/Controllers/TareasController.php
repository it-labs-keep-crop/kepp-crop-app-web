<?php

namespace App\Http\Controllers;

use App\Tarea;
use App\Cultivo;
use App\Proyecto;
use App\Notificacion;
use Illuminate\Http\Request;
use App\Services\TareasService;

class TareasController extends Controller
{
    public function index(Request $request)
    {
      return Tarea::whereHas('cultivo', function($q){
        $q->whereHas('proyecto', function($q){
          $q->where('user_id', request()->user()->id);
        });
      })->with('tipo', 'cultivo.proyecto')
        ->get()
        ->groupBy(function ($tarea, $key) {
          return substr($tarea->fecha, 0, -9);
        });
    }

    public function update(Request $request, Tarea $tarea)
    {
      $request->validate([
        'estado' => 'required|max:255',
        'horas' => 'nullable|numeric',
      ]);

      $tarea->estado = $request->estado;

      $next = Tarea::whereCultivoId($tarea->cultivo_id)
        ->whereDate('fecha', '>', $tarea->fecha)
        ->first();

      // Estados: [posponer, realizada, omitida,]
      if ($request->estado == 'posponer') {
        $before = $tarea->fecha;


        $tarea->fecha = $tarea->fecha->addHours($request->horas);

        if ($tarea->fecha->diffInHours($next->fecha, false) < 12) {
          $tarea->fecha = $before;
          $tarea->estado = 'omitida';
        } else {
          if ($tarea->tipo_tarea_id == 2) {
            Tarea::whereCultivoId($tarea->cultivo_id)
              ->whereDate('fecha', '=', $before)
              ->get()
              ->each(function ($task) use ($request) {
                $task->fecha = $task->fecha->addHours($request->horas);
                $task->estado = 'pospuesta';
                $task->save();
              });
          }
          $tarea->estado = 'pospuesta';
          $tarea->save();

          return response()->json([
            'message' => 'Tareas actualizada correctamente',
          ]);
        }
      }

      $tarea->save();

      if ($tarea->tipo_tarea_id == 2) {
        $same = Tarea::whereCultivoId($tarea->cultivo_id)
          ->whereDate('fecha', '=', $tarea->fecha->subHours($request->horas))
          ->where('id', '>', $tarea->id)
          ->get();

        if ($same->isNotEmpty()) {
          $next = $same->first();
        }
      }

      $tarea->cultivo()->update([
        'tarea_actual' => $next->id,
      ]);

      return response()->json([
        'message' => 'Tareas actualizada correctamente',
      ]);
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }
}
