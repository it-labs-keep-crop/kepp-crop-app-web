<?php

namespace App\Http\Controllers;

use App\Tip;
use App\Cultivo;
use Illuminate\Http\Request;

class TipsController extends Controller
{
    public function index()
    {
      return Tip::whereListado(true)->get();
    }

    public function show(Cultivo $cultivo)
    {
      $tips = Tip::whereListado(false)->first();
      // todo: choose tip random by tipable relationship

      return $tips;
    }
}
