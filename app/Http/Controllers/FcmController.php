<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FcmController extends Controller
{
  public function store(Request $request)
  {
    $request->validate([
      'token' => 'required',
      'device_name' => 'nullable',
    ]);

    $user = User::find($request->user()->id);

    if ($user->fcm_token && $user->fcm_token == $request->token) {
      return;
    }

    $user->fcm_token = $request->token;
    $user->save();
  }
}
