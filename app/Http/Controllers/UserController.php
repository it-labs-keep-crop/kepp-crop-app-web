<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
  public function me(Request $request)
  {
    return  User::find($request->user()->id);
  }

  public function update(User $user, Request $request)
  {
    $data = $request->validate([
        'name' => 'required',
        'email' => 'required|email',
        'avatar' => 'nullable|sometimes|image',
    ]);

    if ($data['avatar']) {
      $data['avatar'] = str_replace('public/', 'storage/', Storage::put('public/avatars', $data['avatar']));
    }

    $user->fill($data)->save();

    return response()->json([
      'message' => 'Usuario actualizado correctamente',
    ]);
  }
}
