<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

class RegisterController extends Controller
{
  public function register(Request $request)
  {
    $data = $request->validate([
      'name' => 'required|max:255',
      'email' => 'required|email',
      'password' => 'required|max:255',
      'avatar' => 'nullable|sometimes|image',
    ]);

    $user = User::whereEmail($request->email)->first();
    if ($user) {
      return response()->json([
        'message' => 'Ya existe un Usuario con ese correo'
      ], 401);
    }

    if ($data['avatar']) {
      $data['avatar'] = str_replace('public/', 'storage/', Storage::put('public/avatars', $data['avatar']));
    }
    $data['password'] = Hash::make($data['password']);

    $user = User::create($data);

    event(new Registered($user));

    return response()->json([
      'message' => 'Usuario creado correctamente',
    ]);
  }

  public function notice()
  {
    return view('auth.verify');
  }

  public function send(Request $request)
  {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('status', 'verification-link-sent');
  }

  public function verify($id, $hash)
  {
    $user = User::find($id);
    $user->markEmailAsVerified();

    return view('auth.verified', compact('user'));

    // $request->fulfill();
    return response()->json([
      'message' => 'Correo verificado correctamente',
    ]);
  }
}
