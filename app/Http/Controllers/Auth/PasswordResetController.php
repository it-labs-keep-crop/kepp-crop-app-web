<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class PasswordResetController extends Controller
{
  public function showForm($from)
  {
    return view('auth.passwords.email', [
      'from' => $from,
    ]);
  }

  public function resetPassword($from, $token)
  {
    return view('auth.passwords.reset', [
      'from' => $from,
      'token' => $token,
    ]);
  }
}
