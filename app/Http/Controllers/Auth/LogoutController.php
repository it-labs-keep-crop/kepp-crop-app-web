<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
  public function logoutWeb()
  {
    Auth::guard('admins')->logout();

    return redirect()->route('login');
  }

  public function logoutApi()
  {
    request()->user()->tokens()->delete();

    return response()->json([
      'message' => 'Ha cerrado sesión correctamente',
    ]);

  }
}

