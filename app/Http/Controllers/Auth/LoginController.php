<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
  public function __invoke(Request $request)
  {
    $request->validate([
      'email'       => 'required|email',
      'password'    => 'required',
      'device_name' => 'required'
    ]);

    $user = User::where('email', $request->email)->first();

    if ($user && !$user->email_verified_at) {
      throw ValidationException::withMessages([
        'email' => ['Dirección de correo no confirmada'],
      ]);
    }

    if (!$user || !Hash::check($request->password, $user->password)) {
      throw ValidationException::withMessages([
        'email' => ['Las credenciales son incorrectas'],
      ]);
    }

    if (!$user->email_verified_at) {
      throw ValidationException::withMessages([
        'email' => ['Dirección de correo no confirmada'],
      ]);
    }

    return response()->json([
      'token' => $user->createToken($request->device_name)->plainTextToken
    ]);
  }
}
