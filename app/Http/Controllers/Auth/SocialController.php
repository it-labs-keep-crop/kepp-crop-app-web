<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserSocialAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialController extends Controller
{
  public function redirectToProvider($driver)
  {
    return Socialite::with($driver)->redirect();
  }

  public function handleProviderCallback($driver)
  {
    $socialUser = Socialite::driver($driver)->user();

    $data = $this->firstOrCreateUser($socialUser);
    Auth::login($data['user']);
    return redirect('/');
  }

  public function handleProviderCallbackMobile(Request $request, $driver)
  {
    $socialUser = Socialite::driver($driver)
      ->userFromToken($request->token);
    $data = $this->firstOrCreateUser($socialUser, $request, $driver);

    if ($data['signed_up']) {
      $data['token'] = $data['user']->createToken($request->device_name)->plainTextToken;
    }

    return response()->json($data);
  }

  protected function firstOrCreateUser(ProviderUser $providerUser, $request, $driver)
  {
    $signedUp = true;
    $account = UserSocialAccount::whereProvider($driver)
      ->whereProviderUserId($providerUser->getId())
      ->first();

    if ($account) {
      $user = $account->user;
    } else {
      $account = new UserSocialAccount([
        'provider_user_id' => $providerUser->getId(),
        'provider' => $driver
      ]);
      $user = User::whereEmail($providerUser->getEmail())->first();
      if (!$user) {
        $user = User::create([
          'name' => $providerUser->getName(),
          'email' => $providerUser->getEmail(),
          'password' => Hash::make($request->token),
          'avatar' => $providerUser->getAvatar(),
        ]);
        // $signedUp = false;
      }
      $account->user()->associate($user);
      $account->save();
    }

    return [
      'user'=> $user,
      'signed_up' => $signedUp,
    ];
  }
}
