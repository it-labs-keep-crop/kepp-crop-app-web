<?php

namespace App\Http\Controllers;

use App\TipoSemilla;
use Illuminate\Http\Request;

class TipoSemillasController extends Controller
{
  public function index()
  {
    return TipoSemilla::wherePublicado(true)
      ->with('etapas')
      ->orderBy('nombre')
      ->get();
  }

  public function show(TipoSemilla $tipoSemilla)
  {
    return $tipoSemilla;
  }
}
