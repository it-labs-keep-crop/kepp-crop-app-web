<?php

namespace App\Http\Controllers;

use App\Notificacion;
use Illuminate\Http\Request;

class NotificacionesController extends Controller
{
    public function index(Request $request)
    {
      return Notificacion::whereUserId($request->user()->id)
        ->with('tipo')
        ->get();
    }
}
