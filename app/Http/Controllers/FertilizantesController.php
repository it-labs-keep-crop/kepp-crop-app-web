<?php

namespace App\Http\Controllers;

use App\Fertilizante;
use Illuminate\Http\Request;

class FertilizantesController extends Controller
{
  public function index()
  {
    return Fertilizante::wherePublicado(true)->get();
  }

  public function show(Fertilizante $fertilizante)
  {
    return $fertilizante;
  }
}
