<?php

namespace App\Http\Controllers;

use App\Etapa;
use App\Maceta;
use App\Semilla;
use App\Proyecto;
use App\TipoSemilla;
use Illuminate\Http\Request;
use App\Services\TareasService;
use Illuminate\Support\Facades\Storage;

class ProyectosController extends Controller
{
  public function index(Request $request)
  {
    return Proyecto::with('etapa')->whereUserId($request->user()->id)
      ->get();
  }

  public function makeTareas(Proyecto $proyecto)
  {
    $proyecto->cultivos->each(function ($cultivo) {
      TareasService::createTareas($cultivo);
    });

    return response()->json([
      'message' => 'Tareas creadas correctamente',
    ]);
  }

  public function create(Request $request)
  {
    if ($request->step == 0) {
      return response()->json([
        'tipos' => TipoSemilla::all(),
        'etapas' => Etapa::all(),
      ]);
    } else if ($request->step == 1) {
      return response()->json([
        'macetas' => Maceta::all(),
        'semillas' => Semilla::all(),
      ]);
    }
      return response()->json([
        'messag' => 'Etapa no es valida',
      ]);
  }

  public function store(Request $request)
  {
    $data = $request->validate([
      'nombre' => 'required|max:255',
      'etapa_id' => 'nullable',
      'tipo_cultivo' => 'required',
      'tipo_semilla_id' => 'required',
      'imagen' => 'nullable|sometimes|image',
    ]);

    if ($request->imagen) {
      $data['imagen'] = str_replace('public/', 'storage/', Storage::put('public/proyectos', $data['imagen']));
    }

    $data['user_id'] = $request->user()->id;
    $proyecto = Proyecto::create($data);

    return response()->json([
      'proyecto' => $proyecto,
      'message' => 'Proyecto creado correctamente',
    ]);
  }

  public function addFertilizante(Proyecto $proyecto, Request $request)
  {
    $request->validate([
      'fertilizante_id' => 'required',
    ]);
    $proyecto->fertilizantes()->attach($request->fertilizante_id);

    return response()->json([
      'message' => 'Fertilizante agregado correctamente',
    ]);
  }

  public function show(Proyecto $proyecto)
  {
    return $proyecto->load('cultivos.tareas');
  }

  public function update(Request $request, $id)
  {
    //
  }

  public function destroy(Proyecto $proyecto)
  {
    $proyecto->delete();
  }
}
