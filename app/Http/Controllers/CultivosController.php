<?php

namespace App\Http\Controllers;

use App\Cultivo;
use App\Proyecto;
use Illuminate\Http\Request;

class CultivosController extends Controller
{
    public function index()
    {
        //
    }

    public function store($proyecto_id, Request $request)
    {
      $request->validate([
        'nombre' => 'required|max:255',
        'semilla_id' => 'nullable',
        'maceta_id' => 'required',
      ]);

      $cultivo = Cultivo::create([
        'nombre' => $request->nombre,
        'semilla_id' => $request->semilla_id,
        'maceta_id' => $request->maceta_id,
        'proyecto_id' => $proyecto_id,
      ]);

      return response()->json([
        'message' => 'Cultivo creado correctamente',
      ]);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
