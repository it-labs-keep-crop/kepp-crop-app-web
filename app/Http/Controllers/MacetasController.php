<?php

namespace App\Http\Controllers;

use App\Maceta;
use Illuminate\Http\Request;

class MacetasController extends Controller
{
  public function index()
  {
    return Maceta::all();
  }

  public function show(Maceta $maceta)
  {
    return $maceta;
  }
}
