<?php

namespace App\Http\Controllers;

use App\Semilla;
use Illuminate\Http\Request;

class SemillasController extends Controller
{
  public function index()
  {
    $tipo_semilla_id = request('tipo_semilla_id');

    return Semilla::wherePublicado(true)
      ->when($tipo_semilla_id, function ($q) use ($tipo_semilla_id) {
        return $q->where('tipo_semilla_id', $tipo_semilla_id);
      })
      ->get();
  }

  public function show(Semilla $semilla)
  {
    return $semilla;
  }
}
