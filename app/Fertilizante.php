<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fertilizante extends Model
{
    protected $fillable = [
        'nombre',
        'marca_id',
        'npk',
        'caracteristicas',
        'formato',
        'unidad_medida',
        'publicado',
        'imagen',
        'disolucion_litro',
        'categoria',
    ];

    protected $casts = [
        'publicado' => 'boolean',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function tips()
    {
        return $this->morphMany(Tip::class, 'tipable');
    }

    public function marca()
    {
        return $this->belongsTo(Marca::class);
    }

    public function tipoSemillas()
    {
        return $this->belongsToMany(Proyecto::class, 'fertilizantes_proyectos');
    }
}
