<?php

namespace App\Services;

use App\User;
use App\Tarea;
use App\Cultivo;
use App\Proyecto;
use App\TipoTarea;
use Carbon\Carbon;
use App\Notificacion;
use App\Notifications\TareasPorHacer;

class TareasService
{
  public static function verificarTareasActuales()
  {
    $today = Carbon::now()->locale('es_CL');

    Proyecto::whereFinalizado(false)
      ->get()
      ->each(function ($proyecto) use ($today) {
        $cultivos = [];
        $proyecto->cultivos->each(function ($cultivo) use ($today, &$cultivos) {
          $diff = $today->diffInHours(Tarea::find($cultivo->tarea_actual)->fecha, false);

          if ($diff == 0) {
            $cultivos[] = $cultivo->nombre;
          }
        });

        if (count($cultivos) > 0) {
          User::find($proyecto->user_id)
            ->notify(new TareasPorHacer(
              Notificacion::create([
                'body' => implode(';', $cultivos),
                'fecha' => $today->toDateTimeString(),
                'user_id' => $proyecto->user_id,
                'proyecto_id' => $proyecto->id,
                'tipo_notificacion_id' => 1,
              ])
            ));
        }
      });
  }

  public static function createTareas(Cultivo $c)
  {
    $cicloVida = $c->semilla->tipoSemilla->ciclo_vida;
    $fRegado = $c->semilla->tipoSemilla->frecuencia_regado;
    $fAbono = $c->semilla->tipoSemilla->frecuencia_fertilizante;

    $tipoRiego = TipoTarea::find(1);
    $descripcionRiego = $tipoRiego->template;
    $minRiego = $tipoRiego->horario_min;
    $maxRiego = $tipoRiego->horario_max;

    $tipoAbono = TipoTarea::find(2);
    $descripcionAbono = $tipoAbono->template;
    $minAbono = $tipoAbono->horario_min;
    $maxAbono = $tipoAbono->horario_max;

    $dayCounter = 0;
    $today = Carbon::today()->locale('es_CL');
    $tempDate = Carbon::createFromDate($today->year, $today->month, $today->day);

    while ($dayCounter < $cicloVida) {
      for ($i = 0; $i < 7; $i++) {
        if ($fAbono > 0 && ($dayCounter % ($fRegado * $fAbono) == 0)) {
          // Tarea regado con Abono
          $c->proyecto->fertilizantes->each(function ($f) use ($tempDate, $c, $minAbono, $maxAbono) {
            Tarea::create([
              'fecha' => $tempDate->toDateString() .' '. rand($minAbono, $maxAbono) .':00:00',
              'estado' => 'por hacer',
              'values' => [
                'cultivo' => $c->nombre,
                'agua' => $c->maceta->agua,
                'disolucion' => $f->disolucion_litro,
                'unidad' => $f->unidad_medida,
                'abono' => $f->nombre .' '. $f->marca,
              ],
              'tipo_tarea_id' => 2,
              'cultivo_id' => $c->id,
            ]);
          });
        } elseif (($dayCounter + $fRegado) % $fRegado == 0) {
          // Tarea regado con Agua
          Tarea::create([
            'fecha' => $tempDate->toDateString() .' '. rand($minRiego, $maxRiego) .':00:00',
            'estado' => 'por hacer',
            'values' => [
              'cultivo' => $c->nombre,
              'agua' => $c->maceta->agua,
            ],
            'tipo_tarea_id' => 1,
            'cultivo_id' => $c->id,
          ]);
        }
        $tempDate->addDay();
        $dayCounter ++;
      }
    }

    $c->tarea_actual = Tarea::where('cultivo_id', $c->id)
      ->orderBy('fecha')
      ->first()
      ->id;
    $c->save();

    return $c->semilla->tipoSemilla;
  }
}

