<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoSemilla extends Model
{
  protected $fillable = [
    'nombre',
    'publicado',
    'ciclo_vida',
    'temperatura_curado_recomendado',
    'tiempo_curado_min',
    'tiempo_curado_max',
    'frecuencia_regado',
    'frecuencia_fertilizante',
  ];

  protected $casts = [
    'publicado' => 'boolean',
  ];

  protected $hidden = [
    'created_at',
    'updated_at',
  ];

  protected $appends = [
    'tiempo_curado',
  ];

  public function getTiempoCuradoAttribute()
  {
    return $this->tiempo_curado_min . ' - ' . $this->tiempo_curado_max;
  }

  public function semillas()
  {
    return $this->hasMany(Semilla::class);
  }

  public function tips()
  {
    return $this->morphMany(Tip::class, 'tipable');
  }

  public function etapas()
  {
    return $this->belongsToMany(Etapa::class, 'etapas_tipo_semillas')->withPivot(
      'dia_inicio_pre_interior',
      'dia_inicio_etapa_interior',
      'dia_inicio_pre_exterior',
      'dia_inicio_etapa_exterior',
    );
  }
}
