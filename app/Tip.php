<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tip extends Model
{
    use HasFactory;

    protected $fillable = [
        'titulo',
        'contenido',
        'listado',
        'link',
        'imagen',
        'tipable_type',
        'tipable_id',
    ];

    protected $casts = [
      'listado' => 'boolean',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function tipable()
    {
        return $this->morphTo();
    }
}
