<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cultivo extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'proyecto_id',
        'semilla_id',
        'maceta_id',
        'tarea_actual',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    protected $with = ['semilla', 'maceta'];

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = ucfirst($value);
    }

    public function proyecto()
    {
        return $this->belongsTo(Proyecto::class);
    }

    public function semilla()
    {
        return $this->belongsTo(Semilla::class);
    }

    public function maceta()
    {
        return $this->belongsTo(Maceta::class);
    }

    public function tareas()
    {
        return $this->hasMany(Tarea::class);
    }
}
