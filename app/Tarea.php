<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $fillable = [
        'values',
        'fecha',
        'estado',
        'cultivo_id',
        'tipo_tarea_id',
    ];

    protected $appends = [
      'descripcion',
      'actual',
    ];

    protected $casts = [
      'values' => 'array',
      'fecha' => 'datetime',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function getDescripcionAttribute()
    {
      $descripcion = $this->tipo->template;

      foreach ($this->values as $key => $value) {
        $descripcion = str_replace('${'.$key.'}', $value, $descripcion);
      }
      return $descripcion;
    }

    public function getActualAttribute()
    {
      if ($this->cultivo->tarea_actual == $this->id) {
        return true;
      }

      return false;
    }

    public function cultivo()
    {
        return $this->belongsTo(Cultivo::class);
    }

    public function tipo()
    {
        return $this->belongsTo(TipoTarea::class, 'tipo_tarea_id');
    }
}
