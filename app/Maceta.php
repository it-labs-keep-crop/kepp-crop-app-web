<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maceta extends Model
{
    protected $fillable = [
        'nombre',
        'capacidad_litros',
        'agua',
        'dimensiones',
        'descripcion',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function tips()
    {
      return $this->morphMany(Tip::class, 'tipable');
    }

    public function cultivos()
    {
        return $this->hasMany(Cultivo::class);
    }
}
