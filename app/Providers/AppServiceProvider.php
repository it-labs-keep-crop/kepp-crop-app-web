<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Auth\Notifications\ResetPassword;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ResetPassword::createUrlUsing(function ($notifiable, string $token) {
          if (class_basename(get_class($notifiable))  == 'User') { // Reset Mobile User
              return env('APP_URL') . 'password/mobile/reset/' . $token;
          }
          return env('APP_URL') . 'password/web/reset/' . $token; // Reset web User (AdminUser)
        });

        View::composer('admin-dashboard', function ($view) {
            $menu = [
                'Mantenedores internos' => [
                    [
                        'label' => 'Regado',
                        'url' => '/admin/regado',
                        'icon' => 'fas-oil-can',
                    ],
                    [
                        'label' => 'Semillas',
                        'url' => '/admin/semillas',
                        'icon' => 'ri-plant-fill',
                    ],
                    [
                        'label' => 'Tipos de Semillas',
                        'url' => '/admin/tiposemillas',
                        'icon' => 'fas-leaf',
                    ],
                    [
                        'label' => 'Fertilizantes',
                        'url' => '/admin/fertilizantes',
                        'icon' => 'bi-droplet-half',
                    ],
                    [
                        'label' => 'Macetas',
                        'url' => '/admin/macetas',
                        'icon' => 'bi-trash2-fill'
                    ],
                    [
                        'label' => 'Marcas',
                        'url' => '/admin/marcas',
                        'icon' => 'fas-tag'
                    ],
                    [
                        'label' => 'Tipo de Tareas',
                        'url' => '/admin/tipotareas',
                        'icon' => 'bi-calendar2-check-fill',
                    ],
                    [
                        'label' => 'Tipo de Notificaciones',
                        'url' => '/admin/tiponotificaciones',
                        'icon' => 'ri-notification-2-fill',
                    ],
                    [
                        'label' => 'Tips',
                        'url' => '/admin/tips',
                        'icon' => 'bi-info-square-fill',
                    ],
                    [
                        'label' => 'Etapas',
                        'url' => '/admin/etapas',
                        'icon' => 'bi-bar-chart-steps',
                    ],
                    [
                        'label' => 'Equipo',
                        'url' => '/admin/users',
                        'icon' => 'bi-people-fill',
                    ],
                ],
                'Mantenedores app' => [
                    [
                        'label' => 'Proyectos',
                        'url' => '/admin/proyectos',
                        'icon' => 'ri-folder-chart-fill',
                    ],
                    [
                        'label' => 'Usuarios',
                        'url' => '/admin/app-users',
                        'icon' => 'fas-user-circle',
                    ],
                ],
                'Extras' => [
                    [
                        'label' => 'Archivos',
                        'url' => '/admin/archivos',
                        'icon' => 'fas-folder',
                    ],
                ],
            ];

            $view->with(compact('menu'));
        });
    }
}
