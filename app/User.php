<?php

namespace App;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
  use HasApiTokens, Notifiable;

  protected $fillable = [
    'name',
    'email',
    'avatar',
    'password',
    'fcm_token',
  ];

  protected $hidden = [
    'password',
    'remember_token',
    'created_at',
    'updated_at',
  ];

  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  public function userSocialAccounts()
  {
    return $this->hasMany(UserSocialAccount::class);
  }

  public function routeNotificationForFcm()
  {
    return $this->fcm_token;
  }

  public function notificaciones()
  {
    return $this->hasMany(Notificacion::class);
  }
}
