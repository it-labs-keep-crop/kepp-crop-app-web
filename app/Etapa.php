<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etapa extends Model
{
    protected $fillable = [
        'nombre',
        'descripcion',
        'icon',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function tips()
    {
        return $this->morphMany(Tip::class, 'tipable');
    }

    public function tipoSemillas()
    {
        return $this->belongsToMany(TipoSemilla::class, 'etapas_tipo_semillas')->withPivot(
          'dia_inicio_pre_interior',
          'dia_inicio_etapa_interior',
          'dia_inicio_pre_exterior',
          'dia_inicio_etapa_exterior',
        );
    }
}
