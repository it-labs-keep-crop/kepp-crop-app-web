<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoNotificacion extends Model
{
    use HasFactory;

    protected $table = 'tipo_notificaciones';

    protected $fillable = [
      'nombre',
      'contenido',
      'icon',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function notificaciones()
    {
        return $this->hasMany(Notificacion::class, 'tipo_notificacion_id');
    }
}
