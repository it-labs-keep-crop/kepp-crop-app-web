<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $fillable = [
        'nombre',
        'tipo_cultivo',
        'user_id',
        'etapa_id',
        'tipo_semilla_id',
        'imagen',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = ucfirst($value);
    }

    public function cultivos()
    {
        return $this->hasMany(Cultivo::class);
    }

    public function tipoSemilla()
    {
        return $this->belongsTo(TipoSemilla::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function etapa()
    {
        return $this->belongsTo(Etapa::class);
    }

    public function fertilizantes()
    {
        return $this->belongsToMany(Fertilizante::class, 'fertilizantes_proyectos');
    }

    public function notificaciones()
    {
        return $this->hasMany(Notificacion::class);
    }
}
