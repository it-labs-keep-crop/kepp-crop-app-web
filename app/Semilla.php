<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semilla extends Model
{
    protected $fillable = [
        'nombre',
        'publicado',
        'marca_id',
        'tipo_semilla_id',
        'thc_porcentaje',
        'caracteristicas',
    ];

    protected $casts = [
        'publicado' => 'boolean',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function tipoSemilla()
    {
        return $this->belongsTo(TipoSemilla::class);
    }

    public function marca()
    {
        return $this->belongsTo(Marca::class);
    }

    public function cultivos()
    {
        return $this->hasMany(Cultivo::class);
    }
}
