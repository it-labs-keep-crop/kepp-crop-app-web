<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Breadcrumb extends Component
{
    public $breadcrumb;
    public $icons;

    public function __construct(array $breadcrumb)
    {
        $this->breadcrumb = $breadcrumb;
        $this->icons = [
          'separator' => 'heroicon-s-chevron-right'
        ];
    }

    public function render()
    {
        return view('components.breadcrumb');
    }
}
