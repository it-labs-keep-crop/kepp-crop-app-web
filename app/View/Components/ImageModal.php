<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ImageModal extends Component
{
    public $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function render()
    {
        return view('components.image-modal');
    }
}
