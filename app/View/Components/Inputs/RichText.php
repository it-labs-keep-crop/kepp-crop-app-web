<?php

namespace App\View\Components\Inputs;

use Illuminate\View\Component;

class RichText extends Component
{
    public $initValue;

    public function __construct($initValue)
    {
        $this->initValue = $initValue;
    }

    public function render()
    {
        return view('components.inputs.rich-text');
    }
}
