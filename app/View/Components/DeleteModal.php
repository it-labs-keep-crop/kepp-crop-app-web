<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DeleteModal extends Component
{
    public $icons = [
        'separator' => '<x-entypo-circle-with-cross class="text-red-500"/>',
    ];

    public function __construct()
    {
    }

    public function render()
    {
        return view('components.delete-modal');
    }
}
