<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoTarea extends Model
{
    use HasFactory;

    protected $fillable = [
        'titulo',
        'template',
        'horario_min',
        'horario_max',
        'variables',
    ];

    protected $casts = [
        'variables' => 'array',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function tareas()
    {
        return $this->hasMany(Tarea::class);
    }
}
