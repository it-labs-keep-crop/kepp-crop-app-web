<?php

namespace App\Console\Commands;

use App\Services\TareasService;
use Illuminate\Console\Command;

class CheckTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica fechas de tareas para notificaciones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        TareasService::verificarTareasActuales();
    }
}
