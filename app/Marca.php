<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'marcable_type',
        'marcable_id',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function marcable()
    {
        return $this->morphTo();
    }
}
