<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Notificacion extends Model
{
    use HasFactory;

    protected $table = 'notificaciones';

    protected $fillable = [
      'body',
      'fecha',
      'user_id',
      'proyecto_id',
      'tipo_notificacion_id',
    ];

    protected $casts = [
      'fecha' => 'datetime',
    ];

    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function proyecto()
    {
        return $this->belongsTo(Proyecto::class);
    }

    public function tipo()
    {
        return $this->belongsTo(TipoNotificacion::class, 'tipo_notificacion_id');
    }
}
