<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Proyecto;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Proyecto::class, 11)->create();
    }
}
