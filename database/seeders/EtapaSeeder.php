<?php

namespace Database\Seeders;

use App\Etapa;
use Illuminate\Database\Seeder;

class EtapaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $etapas = [
        [
          'nombre' => 'Vegetación',
          'descripcion' => 'Esta es una descripción de la etapa, para que todos los usuarios entiendan en que consiste',
        ],
       [
          'nombre' => 'Floración',
          'descripcion' => 'Esta es una descripción de la etapa, para que todos los usuarios entiendan en que consiste',
        ],
        [
          'nombre' => 'Cosecha',
          'descripcion' => 'Esta es una descripción de la etapa, para que todos los usuarios entiendan en que consiste',
        ],
      ];

      foreach ($etapas as $etapa) {
        Etapa::create($etapa);
      }
    }
}
