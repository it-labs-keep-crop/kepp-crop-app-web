<?php

namespace Database\Seeders;

use App\Fertilizante;
use Illuminate\Database\Seeder;

class FertilizanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // factory(Fertilizante::class, 10)->create();

      $fertilizantes = [
        [
          'nombre' => 'Bio Bloom',
          'formato' => 200,
          'unidad_medida' => 'ML',
          'publicado' => true,
          'disolucion_litro' => 10,
          'imagen' => null,
          'categoria' => 'Complemento',
        ],
        [
          'nombre' => 'Black pearl',
          'formato' => 300,
          'unidad_medida' => 'ML',
          'publicado' => true,
          'disolucion_litro' => 5,
          'imagen' => null,
          'categoria' => 'Complemento',
        ],
      ];

      foreach ($fertilizantes as $f) {
        Fertilizante::create($f);
      }
    }
}
