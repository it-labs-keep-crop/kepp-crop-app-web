<?php

namespace Database\Seeders;

use App\TipoTarea;
use Illuminate\Database\Seeder;

class TipoTareaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $tipos = [
        [
          'titulo' => 'Regado con agua',
          'template' => 'Riege su ${cultivo} con ${agua} Litros de agua',
          'horario_min' => 11,
          'horario_max' => 20,
          'variables' => [
            'cultivo' => 'Nombre de un cultivo dentro de un proyecto',
            'agua' => 'Cantidad de agua que debe regar, en Litros',
          ],
        ],
        [
          'titulo' => 'Regado con abono',
          'template' => 'Disuelva ${disolucion} ${unidad} de ${abono} en 1 Litro de agua. Luego riege su ${cultivo} con ${agua} Litros de mezcla',
          'horario_min' => 11,
          'horario_max' => 20,
          'variables' => [
            'cultivo' => 'Nombre de un cultivo dentro de un proyecto',
            'agua' => 'Cantidad de agua que debe regar, en Litros',
            'disolucion' => 'Cantidad de abono por litro de agua',
            'unidad' => 'Unidad de medida para el abono (ml, gr)',
            'abono' => 'Nombre y marca del abono a utilizar',
          ],
        ],
      ];

      foreach ($tipos as $t) {
        TipoTarea::create($t);
      }
    }
}
