<?php

namespace Database\Seeders;

use App\Semilla;
use Illuminate\Database\Seeder;

class SemillaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $semillas = [
        [
          'nombre' => 'Moby Dick',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 3,
        ],
        [
          'nombre' => 'White Widow',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 3,
        ],
        [
          'nombre' => 'Critical+',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 3,
        ],
        [
          'nombre' => 'Original Amnesia',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 3,
        ],
        [
          'nombre' => 'Magnum Auto',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 2,
        ],
        [
          'nombre' => 'Big Devil XL Auto',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 2,
        ],
        [
          'nombre' => 'Radical Auto',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 2,
        ],
        [
          'nombre' => 'Black Jack',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 1,
        ],
        [
          'nombre' => 'Green Poison',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 1,
        ],
        [
          'nombre' => 'Candy Kush Express',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 1,
        ],
        [
          'nombre' => 'Tomate Pomarola',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 4,
        ],
        [
          'nombre' => 'Tomate Cherry',
          'publicado' => true,
          'caracteristicas' => 'Odit libero veritatis cumque. Est possimus aut unde ab deserunt. Sint saepe ut quo veniam quis amet.',
          'thc_porcentaje' => round(rand(100, 300) /10),
          'tipo_semilla_id' => 4,
        ],
      ];

      foreach ($semillas as $s) {
        Semilla::create($s);
      }

      // factory(Semilla::class, 10)->create();
    }
}
