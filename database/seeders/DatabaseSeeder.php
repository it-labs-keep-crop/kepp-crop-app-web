<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(AdminUserSeeder::class);
        $this->call(EtapaSeeder::class);
        $this->call(TipoSemillaSeeder::class);
        $this->call(SemillaSeeder::class);
        $this->call(MacetaSeeder::class);
        $this->call(FertilizanteSeeder::class);
        $this->call(TipoTareaSeeder::class);
        $this->call(TipoNotificacionSeeder::class);
        $this->call(MarcaSeeder::class);
    }
}
