<?php

namespace Database\Seeders;

use App\Maceta;
use Illuminate\Database\Seeder;

class MacetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // factory(Maceta::class, 10)->create();

      $macetas = [
        [
          'nombre' => 'Maceta cuadrada',
          'capacidad_litros' => 20,
          'agua' => 7,
          'dimensiones' => '36x36x37',
          'descripcion' => 'Quia non sapiente quos id molestiae commodi iure. Impedit aut ipsum iste suscipit animi expedita eveniet.',
        ],
        [
          'nombre' => 'Maceta textil',
          'capacidad_litros' => 30,
          'agua' => 10,
          'dimensiones' => '20x20x15',
          'descripcion' => 'Quia non sapiente quos id molestiae commodi iure. Impedit aut ipsum iste suscipit animi expedita eveniet.',
          ],
      ];

      foreach ($macetas as $m) {
        Maceta::create($m);
      }
    }
}
