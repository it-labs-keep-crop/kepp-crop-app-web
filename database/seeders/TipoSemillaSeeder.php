<?php

namespace Database\Seeders;

use App\Etapa;
use App\TipoSemilla;
use Illuminate\Database\Seeder;

class TipoSemillaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $etapas = Etapa::all();

      $tipos = [
        [
          'nombre' => 'Fast Version',
          'publicado' => true,
          'ciclo_vida' => rand(100, 200),
          'temperatura_curado_recomendado' => round(rand(150, 250) / 10),
          'tiempo_curado_min' => rand(5, 10),
          'tiempo_curado_max' => rand(12, 20),
        ],
        [
          'nombre' => 'Autofloreciente',
          'publicado' => true,
          'ciclo_vida' => rand(100, 200),
          'temperatura_curado_recomendado' => round(rand(150, 250) / 10),
          'tiempo_curado_min' => rand(5, 10),
          'tiempo_curado_max' => rand(12, 20),
        ],
        [
          'nombre' => 'Feminizada',
          'publicado' => true,
          'ciclo_vida' => rand(100, 200),
          'temperatura_curado_recomendado' => round(rand(150, 250) / 10),
          'tiempo_curado_min' => rand(5, 10),
          'tiempo_curado_max' => rand(12, 20),
        ],
        [
          'nombre' => 'Tomates',
          'publicado' => true,
          'ciclo_vida' => rand(100, 200),
          'temperatura_curado_recomendado' => round(rand(150, 250) / 10),
          'tiempo_curado_min' => rand(5, 10),
          'tiempo_curado_max' => rand(12, 20),
        ],
      ];

      foreach ($tipos as $tipo) {
        $t = TipoSemilla::create($tipo);
        foreach ($etapas as $etapa) {
          $t->etapas()->attach($etapa->id, [
            'dia_inicio_pre_interior' => rand(10, 200),
            'dia_inicio_etapa_interior' => rand(20, 200),
            'dia_inicio_pre_exterior' => rand(5, 200),
            'dia_inicio_etapa_exterior' => rand(15, 200),
          ]);
        }
      }

      // factory(TipoSemilla::class, 10)->create();
    }
}
