<?php

namespace Database\Seeders;

use App\AdminUser;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      AdminUser::create([
        'name' => 'Admin',
        'email' => 'admin@keepcrop.cl',
        'email_verified_at' => now(),
        'password' => bcrypt('password'),
        'remember_token' => Str::random(10),
      ]);

      factory(AdminUser::class, 3)->create();
    }
}
