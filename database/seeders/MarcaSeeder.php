<?php

namespace Database\Seeders;

use App\Marca;
use App\Semilla;
use App\Fertilizante;
use Illuminate\Database\Seeder;

class MarcaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $marcas = [
        [
          'nombre' => 'Biobizz',
          'marcable_type' => Fertilizante::class,
        ],
       [
          'nombre' => 'Top Crop',
          'marcable_type' => Semilla::class,
        ],
        [
          'nombre' => 'Bio Nova',
          'marcable_type' => Semilla::class,
        ],
        [
          'nombre' => 'Canna',
          'marcable_type' => Semilla::class,
        ],
        [
          'nombre' => 'Grotek',
          'marcable_type' => Fertilizante::class,
        ],
      ];

      foreach ($marcas as $marca) {
        Marca::create($marca);
      }
    }
}
