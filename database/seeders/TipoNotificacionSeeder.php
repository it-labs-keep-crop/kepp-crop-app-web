<?php

namespace Database\Seeders;

use App\TipoNotificacion;
use Illuminate\Database\Seeder;

class TipoNotificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $tipos = [
        [
          'nombre' => 'Regado de proyecto',
          'icon' => 'img/notifications/finalizado.png',
        ],
        [
          'nombre' => 'Proyecto finalizado',
          'icon' => 'img/notifications/regado.png',
        ],
      ];

      foreach ($tipos as $tipo) {
        TipoNotificacion::create($tipo);
      }
    }
}
