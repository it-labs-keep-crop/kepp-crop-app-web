<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Semilla;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(Semilla::class, function (Faker $faker) {
    $tipos = DB::table('tipo_semillas')->select('id')->get()->values();

    return [
      'nombre' => $faker->company,
      // 'publicado' => $faker->randomElement($array = [true, false]),
      'publicado' => false,
      'caracteristicas' => $faker->text($maxNbChars = 200),
      'thc_porcentaje' => round(rand(100, 300) /10),
      'tipo_semilla_id' => $tipos->random()->id,
    ];
});
