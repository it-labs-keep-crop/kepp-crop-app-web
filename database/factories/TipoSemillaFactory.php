<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipoSemilla;
use Faker\Generator as Faker;

$factory->define(TipoSemilla::class, function (Faker $faker) {
    return [
      'nombre' => $faker->company,
      'publicado' => false,
      'ciclo_vida' => rand(100, 200),
      'temperatura_curado_recomendado' => round(rand(150, 250) / 10),
      'tiempo_curado_min' => rand(5, 10),
      'tiempo_curado_max' => rand(12, 20),
    ];
});
