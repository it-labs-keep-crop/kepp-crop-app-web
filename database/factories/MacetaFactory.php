<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Maceta;
use Faker\Generator as Faker;

$factory->define(Maceta::class, function (Faker $faker) {
    $capacidad = rand(5, 50);

    return [
        'nombre' => 'Maceta ' . $faker->randomDigit,
        'agua' => round($capacidad / 7, 1),
        'capacidad_litros' => $capacidad,
        'dimensiones' => rand(18, 40) .'x'. rand(18, 40) .'x'. rand(18, 40),
        'descripcion' => $faker->text($maxNbChars = 150),
    ];
});
