<?php

namespace Database\Factories;

use App\Proyecto;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(Proyecto::class, function (Faker $faker) {
    $tipos = DB::table('tipo_semillas')->select('id')->get()->values();
    $users = DB::table('users')->select('id')->get()->values();
    $etapas = DB::table('etapas')->select('id')->get()->values();

    return [
        'nombre' => $faker->name,
        'tipo_cultivo' => $faker->randomElement($array = [
            'Transgénico',
            'Hidropónico',
            'Organopónico',
            'Tradicional',
        ]),
        'user_id' => $users->random()->id,
        'etapa_id' => $etapas->random()->id,
        'tipo_semilla_id' => $tipos->random()->id,
    ];
});
