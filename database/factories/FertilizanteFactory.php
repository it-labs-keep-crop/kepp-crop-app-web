<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Fertilizante;
use Faker\Generator as Faker;

$factory->define(Fertilizante::class, function (Faker $faker) {
    $unidad_medida = $faker->randomElement($array = ['ML', 'GR']);

    return [
        'nombre' => $faker->randomElement($array = ['Top max','Black pearl','Bio heaven', 'Bio grow', 'Monster bloom', 'Root']),
        'marca' => $faker->randomElement($array = ['Biobizz','Topcrop','Grotek']),
        'formato' => $unidad_medida == 'ML' ? rand(1, 40) * 250 : rand(1, 10) * 5,
        'unidad_medida' => $unidad_medida,
        'publicado' => $faker->randomElement($array = [true, false]),
        'disolucion_litro' => rand(2, 10),
        'categoria' => $faker->randomElement($array = ['Complemento','Vegetación','Floración']),
    ];
});
