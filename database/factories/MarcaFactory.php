<?php

namespace Database\Factories;

use App\Marca;
use App\Semilla;
use App\Fertilizante;
use Illuminate\Database\Eloquent\Factories\Factory;

class MarcaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Marca::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
          'nombre' => $faker->randomElement($array = ['Biobizz','Topcrop','Grotek', 'Canna', 'Atami']),
          'marcable_type' => $faker->randomElement($array = [
            Semilla::class,
            Fertilizante::class,
          ]),
        ];
    }
}
