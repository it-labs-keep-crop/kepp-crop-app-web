<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('tipo_cultivo');
            $table->boolean('finalizado')->default(false);
            $table->double('cosecha_gramos')->nullable();
            $table->string('imagen')->nullable();
            $table->foreignId('user_id');
            $table->foreignId('etapa_id');
            $table->foreignId('tipo_semilla_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
