<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFertilizantesProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fertilizantes_proyectos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('proyecto_id');
            $table->foreignId('fertilizante_id');
            // $table->integer('cantidad'); // maybe not
            // $table->integer('dosis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fertilizantes_proyectos');
    }
}
