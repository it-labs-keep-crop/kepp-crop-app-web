<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNpkAndCaracteristicasColumnsToFertilizantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fertilizantes', function (Blueprint $table) {
            $table->string('npk')->nullable();
            $table->text('caracteristicas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fertilizantes', function (Blueprint $table) {
            $table->dropColumn('npk');
            $table->dropColumn('caracteristicas');
        });
    }
}
