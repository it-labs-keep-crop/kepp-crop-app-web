<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtapasTipoSemillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etapas_tipo_semillas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('etapa_id');
            $table->foreignId('tipo_semilla_id');
            $table->integer('dia_inicio_pre_interior')->nullable();
            $table->integer('dia_inicio_etapa_interior')->nullable();
            $table->integer('dia_inicio_pre_exterior')->nullable();
            $table->integer('dia_inicio_etapa_exterior')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etapas_tipo_semillas');
    }
}
