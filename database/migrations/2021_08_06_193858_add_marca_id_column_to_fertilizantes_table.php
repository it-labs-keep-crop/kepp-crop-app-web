<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMarcaIdColumnToFertilizantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fertilizantes', function (Blueprint $table) {
            $table->dropColumn('marca');
            $table->foreignId('marca_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fertilizantes', function (Blueprint $table) {
            $table->string('marca')->nullable();
            $table->dropColumn('marca_id');
        });
    }
}
