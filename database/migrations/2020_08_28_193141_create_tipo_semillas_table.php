<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoSemillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_semillas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->boolean('publicado');
            $table->float('temperatura_curado_recomendado')->nullable();
            $table->integer('ciclo_vida');
            $table->integer('tiempo_curado_min')->nullable();
            $table->integer('tiempo_curado_max')->nullable();
            $table->integer('frecuencia_regado')->default(2);
            $table->integer('frecuencia_fertilizante')->default(4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_semillas');
    }
}
