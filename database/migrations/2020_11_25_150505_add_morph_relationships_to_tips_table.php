<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMorphRelationshipsToTipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tips', function (Blueprint $table) {
            $table->dropColumn('etapa_id');
            $table->boolean('listado');
            $table->foreignId('tipable_id')->nullable();
            $table->string('tipable_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tips', function (Blueprint $table) {
            $table->foreignId('etapa_id')->nullable();
            $table->dropColumn('listado');
            $table->dropColumn('tipable_id');
            $table->dropColumn('tipable_type');
        });
    }
}
