<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\LoginController')->name('login');

Route::post('register', 'Auth\RegisterController@register')->name('user.register');
Route::get('register/verify', 'Auth\RegisterController@notice')->name('verification.notice');
Route::get('/register/verify/{id}/{hash}', 'Auth\RegisterController@verify')
  ->name('verification.verify');
Route::post('/register/verification-notification', 'Auth\RegisterController@send')
  ->name('verification.send');

Route::group(['middleware' => 'auth:sanctum'], function () {

  Route::post('logout', 'Auth\LogoutController@logoutApi')->name('logout.api');

  Route::post('fcm', 'FcmController@store');

  // Datos del usuario logeado ...
  Route::get('me', 'UserController@me');

  Route::get('proyectos', 'ProyectosController@index');
  Route::post('proyectos', 'ProyectosController@store');
  Route::get('proyectos/create', 'ProyectosController@create');
  Route::get('proyectos/{proyecto}', 'ProyectosController@show');
  Route::post('proyectos/{proyecto}/fertilizantes', 'ProyectosController@addFertilizante');
  Route::post('proyectos/{proyecto}/cultivos', 'CultivosController@store');
  Route::post('proyectos/{proyecto}/tareas', 'ProyectosController@makeTareas');

  Route::get('tiposemillas', 'TipoSemillasController@index');
  Route::get('tiposemillas/{tipoSemilla}', 'TipoSemillasController@show');

  Route::get('tareas', 'TareasController@index');
  Route::post('tareas/{tarea}', 'TareasController@update');

  Route::get('semillas', 'SemillasController@index');
  Route::get('semillas/{semilla}', 'SemillasController@show');

  Route::get('fertilizantes', 'FertilizantesController@index');

  Route::get('macetas', 'MacetasController@index');

  Route::get('etapas', 'EtapasController@index');

  Route::get('notificaciones', 'NotificacionesController@index');

  Route::post('users/{user}', 'UserController@update');

  Route::get('tips', 'TipsController@index');
  Route::get('tips/{cultivo}', 'TipsController@show');
});

// Socialite
Route::group(['prefix' => 'social'], function () {
    Route::post('{driver}/callback', 'Auth\SocialController@handleProviderCallbackMobile');
});

