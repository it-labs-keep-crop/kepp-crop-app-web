<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Riego\Riego;
use App\Http\Livewire\Admin\Users\Users;
use App\Http\Livewire\Admin\Files\Files;
use App\Http\Livewire\Admin\Marcas\Marcas;
use App\Http\Livewire\Admin\Etapas\Etapas;
use App\Http\Livewire\Admin\Macetas\Macetas;
use App\Http\Livewire\Admin\Users\UserCreate;
use App\Http\Livewire\Admin\Users\UserDetail;
use App\Http\Livewire\Admin\Files\FileCreate;
use App\Http\Livewire\Admin\Files\FileDetail;
use App\Http\Livewire\Admin\Riego\RiegoDetail;
use App\Http\Livewire\Admin\AppUsers\AppUsers;
use App\Http\Livewire\Admin\Semillas\Semillas;
use App\Http\Livewire\Admin\Marcas\MarcaCreate;
use App\Http\Livewire\Admin\Marcas\MarcaDetail;
use App\Http\Livewire\Admin\Etapas\EtapaCreate;
use App\Http\Livewire\Admin\Etapas\EtapaDetail;
use App\Http\Livewire\Admin\Proyectos\Proyectos;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Livewire\Admin\Macetas\MacetaCreate;
use App\Http\Livewire\Admin\Macetas\MacetaDetail;
use App\Http\Livewire\Admin\Semillas\SemillaCreate;
use App\Http\Livewire\Admin\Semillas\SemillaDetail;
use App\Http\Livewire\Admin\AppUsers\AppUserCreate;
use App\Http\Livewire\Admin\AppUsers\AppUserDetail;
use App\Http\Livewire\Admin\Proyectos\ProyectoCreate;
use App\Http\Livewire\Admin\Proyectos\ProyectoDetail;
use App\Http\Controllers\Auth\PasswordResetController;
use App\Http\Livewire\Admin\TipoSemillas\TipoSemillas;
use App\Http\Livewire\Admin\Fertilizantes\Fertilizantes;
use App\Http\Livewire\Admin\TipoSemillas\TipoSemillaCreate;
use App\Http\Livewire\Admin\TipoSemillas\TipoSemillaDetail;
use App\Http\Livewire\Admin\Fertilizantes\FertilizanteCreate;
use App\Http\Livewire\Admin\Fertilizantes\FertilizanteDetail;

use App\Http\Livewire\Admin\TipoTareas\TipoTareas;
use App\Http\Livewire\Admin\TipoTareas\TipoTareaCreate;
use App\Http\Livewire\Admin\TipoTareas\TipoTareaDetail;

use App\Http\Livewire\Admin\Tips\Tips;
use App\Http\Livewire\Admin\Tips\TipCreate;
use App\Http\Livewire\Admin\Tips\TipDetail;

use App\Http\Livewire\Admin\TipoNotificaciones\TipoNotificaciones;
use App\Http\Livewire\Admin\TipoNotificaciones\TipoNotificacionCreate;
use App\Http\Livewire\Admin\TipoNotificaciones\TipoNotificacionDetail;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware('guest')->group(function () {
    Route::view('login', 'auth.login')->name('login');
    Route::view('register', 'auth.register')->name('register');
});
Route::get('password/{from}/reset', [PasswordResetController::class, 'showForm'])->name('password.request');
Route::get('password/{from}/reset/{token}', [PasswordResetController::class, 'resetPassword'])->name('password.reset');

Route::group(['middleware' => 'auth:admins'], function () {

  Route::post('logout', [LogoutController::class, 'logoutWeb'])->name('logout');

  Route::get('admin/dashboard', Dashboard::class)->name('dashboard');
  Route::get('admin/regado', Riego::class)->name('regado');
  Route::get('admin/regado/{tipoSemilla}', RiegoDetail::class)->name('regado.detail');

  Route::get('admin/users', Users::class)->name('users');
  Route::get('admin/users/create', UserCreate::class)->name('users.create');
  Route::get('admin/users/{adminUser}', UserDetail::class)->name('users.detail');

  Route::get('admin/tiposemillas', TipoSemillas::class)->name('tiposemillas');
  Route::get('admin/tiposemillas/create', TipoSemillaCreate::class)->name('tiposemillas.create');
  Route::get('admin/tiposemillas/{tipoSemilla}', TipoSemillaDetail::class)->name('tiposemillas.detail');

  Route::get('admin/macetas', Macetas::class)->name('macetas');
  Route::get('admin/macetas/create', MacetaCreate::class)->name('macetas.create');
  Route::get('admin/macetas/{maceta}', MacetaDetail::class)->name('macetas.detail');

  Route::get('admin/marcas', Marcas::class)->name('marcas');
  Route::get('admin/marcas/create', MarcaCreate::class)->name('marcas.create');
  Route::get('admin/marcas/{marca}', MarcaDetail::class)->name('marcas.detail');

  Route::get('admin/fertilizantes', Fertilizantes::class)->name('fertilizantes');
  Route::get('admin/fertilizantes/create', FertilizanteCreate::class)->name('fertilizantes.create');
  Route::get('admin/fertilizantes/{fertilizante}', FertilizanteDetail::class)->name('fertilizantes.detail');

  Route::get('admin/semillas', Semillas::class)->name('semillas');
  Route::get('admin/semillas/create', SemillaCreate::class)->name('semillas.create');
  Route::get('admin/semillas/{semilla}', SemillaDetail::class)->name('semillas.detail');

  Route::get('admin/tipotareas', TipoTareas::class)->name('tipotareas');
  Route::get('admin/tipotareas/create', TipoTareaCreate::class)->name('tipotareas.create');
  Route::get('admin/tipotareas/{tipoTarea}', TipoTareaDetail::class)->name('tipotareas.detail');

  Route::get('admin/tips', Tips::class)->name('tips');
  Route::get('admin/tips/create', TipCreate::class)->name('tips.create');
  Route::get('admin/tips/{tip}', TipDetail::class)->name('tips.detail');

  Route::get('admin/etapas', Etapas::class)->name('etapas');
  Route::get('admin/etapas/create', EtapaCreate::class)->name('etapas.create');
  Route::get('admin/etapas/{etapa}', EtapaDetail::class)->name('etapas.detail');

  Route::get('admin/archivos', Files::class)->name('files');
  Route::get('admin/archivos/create', FileCreate::class)->name('files.create');
  Route::get('admin/archivos/{file}', EtapaDetail::class)->name('files.detail');

  Route::get('admin/app-users', AppUsers::class)->name('app-users');
  Route::get('admin/app-users/create', AppUserCreate::class)->name('app-users.create');
  Route::get('admin/app-users/{user}', AppUserDetail::class)->name('app-users.detail');

  Route::get('admin/proyectos', Proyectos::class)->name('proyectos');
  Route::get('admin/proyectos/create', ProyectoCreate::class)->name('proyectos.create');
  Route::get('admin/proyectos/{proyecto}', ProyectoDetail::class)->name('proyectos.detail');

  Route::get('admin/tiponotificaciones', TipoNotificaciones::class)->name('tipoNotificaciones');
  Route::get('admin/tiponotificaciones/create', TipoNotificacionCreate::class)->name('tipoNotificaciones.create');
  Route::get('admin/tiponotificaciones/{tipo}', TipoNotificacionDetail::class)->name('tipoNotificaciones.detail');

});

// Socialite
Route::group(['prefix' => 'social'], function () {
    Route::get('facebook/redirect', 'Auth\SocialController@redirectToProvider');
    Route::get('facebook/callback', 'Auth\SocialController@handleProviderCallback');
});

