@if ($showSearch)
    <div class="relative flex rounded-md shadow-sm">
        <input
            wire:model{{ $this->searchFilterOptions }}="filters.search"
            placeholder="Buscar"
            type="text"
            class="flex-3 px-4 shadow-sm border-gray block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 focus:outline-none focus:border-indigo-300 focus:shadow-outline-indigo @if (isset($filters['search']) && strlen($filters['search'])) rounded-none rounded-l-md @else rounded-md @endif"
        >
        @if (!isset($filters['search']) || !strlen($filters['search']))
          <div class="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
            <x-css-search class="text-gray-400" />
          </div>
        @endif
        </input>


        @if (isset($filters['search']) && strlen($filters['search']))
            <span wire:click="$set('filters.search', null)" class="inline-flex items-center px-3 text-gray-500 border border-l-0 border-gray-300 cursor-pointer rounded-r-md bg-gray-50 sm:text-sm">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </span>
        @endif
    </div>
@endif
