@extends('layouts.auth')
@section('title', 'Create a new account')

@push('css')
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css' rel='stylesheet' />
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css" type="text/css">

    {{-- mapbox geocoder --}}
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.js'></script>
    <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
    <!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>

    <style>
    .mapboxgl-ctrl-geocoder {
        min-width: 100%;
        background-color: transparent;
    }
    .mapboxgl-ctrl-geocoder--input {
        color: white;
        border-width: 1px;
        border-radius: 0.375rem;
        border-color: rgba(63, 66, 75, var(--border-opacity));
        background-color: rgba(63, 66, 75, var(--bg-opacity));
    }
    .mapboxgl-ctrl-geocoder--input:focus {
        color: white;
    }
    .mapbox-gl-geocoder--no-results {
        content: 'No se encontraron resultados';
    }
    </style>

@endpush

@section('content')
    <div>
        @livewire('auth.register')
    </div>
@endsection
