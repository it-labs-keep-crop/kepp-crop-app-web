@extends('layouts.auth')
@section('title', 'Correo confirmado')

@section('content')
  <div>
    <div class="sm:mx-auto sm:w-full sm:max-w-md">
      {{-- <x-logo class="w-auto h-16 mx-auto text-indigo-600" /> --}}

        <h2 class="mt-6 text-3xl font-extrabold text-center text-gray-900 leading-9">
          Correo confirmado
        </h2>

    </div>

    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
      <div class="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10">

        <div class="text-sm text-gray-700">
          <p>Bienvenido, <b>{{ $user->name }}.</b></p>
          <br/>
          <p>Tu correo: <i>{{ $user->email }}</i> ha sido confirmado correctamente</p>

          <p class="mt-3"> Ya puedes comenzar a usar KeepCrop. Abre tu aplicación e inicia sesión</p>
        </div>
      </div>
    </div>
  </div>

@endsection
