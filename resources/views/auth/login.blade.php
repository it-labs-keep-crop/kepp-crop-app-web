@extends('layouts.auth')
@section('title', 'Iniciar Sesion')

@section('content')
    <div>
        @livewire('auth.login')
    </div>
@endsection
