<div>
    <div class="sm:mx-auto sm:w-full sm:max-w-md">

        <h2 class="text-3xl font-extrabold text-center text-gray-900 dark:text-gray-100 leading-9">
            Iniciar Sesión
        </h2>
    </div>

    <div class="mx-4 mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div class="px-4 py-8 bg-white border border-gray-100 shadow dark:bg-gray-900 dark:border-gray-800 sm:rounded-lg sm:px-10">
            <form wire:submit.prevent="authenticate">
                <div>
                    <label for="email" class="block text-sm font-medium text-gray-700 dark:text-gray-300 leading-5">
                        Email
                    </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <input wire:model.lazy="email" id="email" name="email" type="email" required class="outline-none bg-gray-100 dark:bg-gray-800 text-gray-800 dark:text-white block w-full px-3 py-2 rounded-md placeholder-gray-400 sm:text-sm sm:leading-5 @error('email') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror" />
                    </div>

                    @error('email')
                        <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mt-6">
                    <label for="password" class="block text-sm font-medium text-gray-700 dark:text-gray-300 leading-5">
                        Contraseña
                    </label>

                    <div class="mt-1 rounded-md shadow-sm">
                        <input wire:model.lazy="password" id="password" type="password" required class="outline-none bg-gray-100 dark:bg-gray-800 text-gray-800 dark:text-white block w-full px-3 py-2 rounded-md placeholder-gray-400 sm:text-sm sm:leading-5 @error('password') border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red @enderror" />
                    </div>

                    @error('password')
                        <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                </div>

                <div class="flex items-center justify-between mt-6">
                    <div class="flex items-center">
                        <input wire:model.lazy="remember" id="remember" type="checkbox" class="w-4 h-4 text-indigo-600 form-checkbox transition duration-150 ease-in-out" />
                        <label for="remember" class="block ml-2 text-sm text-gray-500 leading-5">
                            Recordar
                        </label>
                    </div>

                    <div class="text-sm leading-5">
                      <a href="{{ route('password.request', ['from' => 'web']) }}" class="font-medium text-indigo-600 dark:text-indigo-400 hover:text-indigo-800 focus:outline-none focus:underline transition ease-in-out duration-150">
                            Olvidaste tu contraseña?
                        </a>
                    </div>
                </div>

                <div class="mt-6">
                    <span class="block w-full rounded-md shadow-sm">
                        <button type="submit" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-700 focus:outline-none focus:border-indigo-700 focus:shadow-outline active:bg-indigo-700 transition duration-150 ease-in-out">
                            Ingresar
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
