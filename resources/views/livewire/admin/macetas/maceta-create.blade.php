<div class="pt-2 pb-6 md:py-6">
  <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
    <div class="md:flex md:items-center md:justify-between">
      <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
    </div>
  </div>
  <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
    <div class="py-4">
      <form class="flex flex-col" id="edit-user" wire:submit.prevent="store">
        <div class="py-2 -my-2 overflow-x-auto sm:px-6 lg:-mx-6 lg:px-6">
          <div class="inline-block min-w-full overflow-hidden align-middle bg-white border-b border-gray-200 shadow sm:rounded-lg">
            <div class="mx-6">

              <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:pt-5">
                <label for="nombre" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Nombre
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <input id="nombre" wire:model="nombre" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                  </div>
                  <x-error field="nombre" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="capacidad_litros" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Capacidad (litros)
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <input
                      type="number"
                      id="capacidad_litros"
                      wire:model="capacidad_litros"
                      class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                  </div>
                  <x-error field="capacidad_litros" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="agua" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Agua
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <input
                      id="agua"
                      type="number"
                      wire:model="agua"
                      class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                  </div>
                  <x-error field="agua" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-6 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="dimensiones" class="block text-sm font-medium text-gray-700 sm:col-span-2 leading-5 sm:mt-px sm:pt-2">
                  Dimensiones
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-1 grid grid-cols-3">
                  <div class="col-span-2 rounded-md shadow-sm">
                    <input
                      type="number"
                      wire:model="dimensiones.alto"
                      class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                      />
                    <x-error field="dimensiones.alto" class="mt-2 text-xs italic text-red-500" />
                  </div>
                  <span class="mx-auto mt-3 col-span-1">
                    x
                  </span>
                </div>
                <div class="mt-1 sm:mt-0 sm:col-span-1 grid grid-cols-3">
                  <div class="col-span-2 rounded-md shadow-sm">
                    <input
                      type="number"
                      wire:model="dimensiones.ancho"
                      class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                      />
                    <x-error field="dimensiones.ancho" class="mt-2 text-xs italic text-red-500" />
                  </div>
                  <span class="mx-auto mt-3 col-span-1">
                    x
                  </span>
                </div>
                <div class="mt-1 sm:mt-0 sm:col-span-1 grid grid-cols-3">
                  <div class="col-span-2 rounded-md shadow-sm">
                    <input
                      type="number"
                      wire:model="dimensiones.largo"
                      class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                      />
                    <x-error field="dimensiones.largo" class="mt-2 text-xs italic text-red-500" />
                  </div>
                  <span class="mx-auto mt-3 col-span-1">
                    cm
                  </span>
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="descripcion" class="block text-sm font-medium text-gray-700 leading-5">
                  Descripción
                </label>
                <div class="mt-2 sm:mt-0 sm:col-span-2">
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                      <textarea
                        rows="3"
                        id="descripcion"
                        wire:model="descripcion"
                        class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                      </textarea>
                    </div>
                    <x-error field="descripcion" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="pt-5 mt-8">
          <div class="flex justify-end">
            <span class="inline-flex rounded-md shadow-sm">
              <button type="button" wire:click="cancel"
                class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md leading-5 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                Cancelar
              </button>
            </span>
            <span class="inline-flex ml-3 rounded-md shadow-sm">
              <button type="submit" form="edit-user"
                class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                Guardar
              </button>
            </span>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
