<div class="container">
  <x-toast type="success" />
  <x-delete-modal>
    <x-slot name="title">Confirmación</x-slot>
    <x-slot name="message">¿Está seguro de eliminar este archivo?</x-slot>
  </x-delete-modal>


  <div class="pt-2 pb-6 md:py-6">
    <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
      <div class="md:flex md:items-center md:justify-between">
        <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
      </div>
    </div>
    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
      <div class="py-4">

        <div class="flex justify-between pb-4">
          <div class="flex w-2/3 mr-auto">
            <div class="relative w-full mt-1 rounded-md shadow-sm">
              <input id="search" wire:model='q' class="block w-full pr-10 form-input sm:text-sm sm:leading-5" placeholder="Buscar">
              <div class="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
                <x-css-search class="text-gray-400" />
              </div>
            </div>
          </div>
          <div class="flex w-1/3">
            <span class="ml-auto shadow-sm rounded-md">
              <a
                type="button"
                href="{{ route('files.create') }}"
                class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out"
                >
                Crear archivo
              </a>
            </span>
          </div>
        </div>

        <div class="flex flex-col">
          <div class="my-2 overflow-x-auto border-b border-gray-200 rounded-lg">
            <table class="w-full border-b border-gray-200 rounded-lg table-fixed divide-y divide-gray-200">
              <thead>
                <tr>
                  <th class="w-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                  <th class="w-64 py-3 pl-6 text-xs font-medium tracking-wider text-left text-gray-500 uppercase break-words whitespace-normal border-b border-gray-200 bg-gray-50 leading-4">
                    Nombre
                  </th>
                  <th class="w-5 py-3 pr-3 border-b border-gray-200 bg-gray-50"></th>
                  <th class="w-6 py-3 pr-3 border-b border-gray-200 bg-gray-50"></th>
                </tr>
              </thead>
              <tbody class="bg-white divide-y divide-gray-200">
                @foreach ($files as $file)
                  <tr class="hover:bg-gray-100">
                    <td class="px-6 py-4 text-sm font-medium text-gray-900 whitespace-no-wrap leading-5">
                      {{ svg($this->getIcon($file->extension))->class($this->getClass($file->extension)) }}
                    </td>
                    <td class="px-6 py-4 text-sm font-medium text-gray-900 whitespace-no-wrap leading-5">
                      {{ $file->name .'.'. $file->extension }}
                    </td>
                    <td class="px-6 py-4 text-sm font-medium text-right whitespace-no-wrap border-b border-gray-200 leading-5">
                      <button
                      type="button"
                      wire:click="download({{ $file->id }})"
                      class="text-indigo-500 focus:outline-none hover:text-indigo-700"
                      >
                        <x-heroicon-o-download class="w-5 h-5"/>
                      </button>
                    </td>
                    <td class="px-6 py-4 text-sm font-medium text-right whitespace-no-wrap border-b border-gray-200 leading-5">
                      <button
                      type="button"
                      wire:click="handleDestroy({{ $file->id }})"
                      class="text-red-500 focus:outline-none hover:text-red-700"
                      >
                        <x-far-trash-alt class="w-5 h-5"/>
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          {{ $files->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
