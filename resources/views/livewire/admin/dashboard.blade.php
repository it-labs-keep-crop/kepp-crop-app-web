<div class="m-12">
  <div class="bg-white shadow sm:rounded-lg">
    <div class="px-4 py-5 sm:p-6">
      <h3 class="text-lg font-medium text-gray-900 leading-6">
        Bienvenido {{ Auth::user()->name }}
      </h3>
      <div class="max-w-xl mt-2 text-sm text-gray-500 leading-5">
        <p>
        Al sistema de administración para la aplicación Móvil KeepCrop
        </p>
      </div>
    </div>
  </div>
</div>
