<div class="container">
  <x-toast type="success" />
  <x-delete-modal>
    <x-slot name="title">Confirmación</x-slot>
    <x-slot name="message">¿Está seguro de eliminar el usuario?</x-slot>
  </x-delete-modal>

  <div class="pt-2 pb-6 md:py-6" x-data="{editable: false, modal: false}" x-cloak>
    <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
      <div class="md:flex md:items-center md:justify-between">
        <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
        <div class="flex mt-4 md:mt-0 md:ml-4" x-show="!editable">
          @if (!$isLogged)
          <span class="shadow-sm rounded-md">
            <button
              type="button"
              wire:click="$emit('open-modal')"
              class="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:text-gray-800 active:bg-gray-50 transition duration-150 ease-in-out"
              >
              Eliminar
            </button>
          </span>
          @endif
          <span class="ml-3 shadow-sm rounded-md">
            <button type="button" @click="editable = true" class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
              Editar
            </button>
          </span>
        </div>

      </div>
    </div>
    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
      <div class="py-4">
        <form class="flex flex-col" id="edit-user" wire:submit.prevent="storeUser">
          <div class="py-2 -my-2 overflow-x-auto sm:px-6 lg:-mx-6 lg:px-6">
            <div class="inline-block min-w-full overflow-hidden align-middle bg-white border-b border-gray-200 shadow sm:rounded-lg">
              <div class="mx-6">
                <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:pt-5">
                  <label for="name" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Nombre
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="max-w-lg rounded-md shadow-sm" x-show="editable">
                      <input id="name" wire:model="name" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    </div>
                    <x-error field="name" class="mt-2 text-xs italic text-red-500" />
                    <p class="text-m" x-show="!editable">
                      {{ $user->name }}
                    </p>
                  </div>
                </div>

                <div class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="email" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Email
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="flex max-w-lg rounded-md shadow-sm" x-show="editable">
                      <input id="email" wire:model="email" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    </div>
                    <x-error field="email" class="mt-2 text-xs italic text-red-500" />
                    <p class="text-sm" x-show="!editable">
                      {{ $email }}
                    </p>
                  </div>
                </div>

                <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="photo" class="block text-sm font-medium text-gray-700 leading-5">
                    Avatar
                  </label>
                  <div class="mt-2 sm:mt-0 sm:col-span-2">
                    <div class="text-sm">
                      @if ($avatar)
                        <x-image-modal :path="$avatar->temporaryUrl()">
                          <img class="w-24 h-24 overflow-hidden rounded-md" src="{{ $avatar->temporaryUrl() }}">
                        </x-image-modal>
                      @else
                        @if ($user->avatar)
                          <x-image-modal :path="asset($user->avatar)">
                            <img class="w-24 h-24 overflow-hidden rounded-md" src="{{ asset($user->avatar) }}">
                          </x-image-modal>
                        @else
                          <x-image-modal :path="asset('img/user.png')">
                            <img class="w-24 h-24 overflow-hidden rounded-md" src="{{ asset('img/user.png') }}">
                          </x-image-modal>
                        @endif
                      @endif
                    </div>

                    <div class="flex max-w-lg rounded-md shadow-sm" x-show="editable">
                      <input type="file" wire:model="avatar" x-ref="imagen">
                    </div>
                    <x-error field="avatar" class="mt-2 text-xs italic text-red-500" />

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="pt-5 mt-8 border-t border-gray-200" x-show="editable">
            <div class="flex justify-end">
              <span class="inline-flex rounded-md shadow-sm">
                <button type="button" wire:click="cancel" class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md leading-5 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                  Cancelar
                </button>
              </span>
              <span class="inline-flex ml-3 rounded-md shadow-sm">
                <button type="submit" form="edit-user" class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                  Guardar
                </button>
              </span>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
