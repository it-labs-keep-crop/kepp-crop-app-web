<div class="pt-2 pb-6 md:py-6">

  <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
    <div class="md:flex md:items-center md:justify-between">
      <div class="flex-1 min-w-1">
        <h3 class="text-xl font-bold text-gray-900 leading-7 sm:text-xl sm:leading-9 sm:truncate">
          {{ $title }}
        </h3>
      </div>
    </div>
  </div>
  <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
    <div class="py-4">
      <form class="flex flex-col" id="create-tiposemilla" wire:submit.prevent="storeTipoSemilla">
        <div class="py-2 -my-2 overflow-x-auto sm:px-6 lg:-mx-6 lg:px-6">
          <div class="inline-block min-w-full overflow-hidden align-middle bg-white border-b border-gray-200 shadow sm:rounded-lg">
            <div class="mx-6">

              <div class="my-6 sm:mt-5 sm:items-start sm:pb-5">
                <h3 class="block font-bold text-gray-700 text-md sm:mt-px sm:pt-2">
                  Tipo De Semilla
                </h3 >
              </div>

              <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200">
                <label for="name" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Nombre
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <input id="nombre" wire:model="nombre" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                  </div>
                  <x-error field="nombre" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="publicado" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Publicado
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <select id="publicado" wire:model="publicado" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                      <option value="0">Borrador</option>
                      <option value="1">Publicado</option>
                    </select>
                  </div>
                  <x-error field="publicado" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-10 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="ciclo_vida" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Ciclo de vida (días)
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <input
                      type="number"
                      id="ciclo_vida"
                      wire:model="ciclo_vida"
                      class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                      />
                  </div>
                  <x-error field="ciclo_vida" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-10 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="temperatura_curado_recomendado" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Curado recomendado (°C)
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <input
                      type="number"
                      id="temperatura_curado_recomendado"
                      wire:model="temperatura_curado_recomendado"
                      class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                      />
                  </div>
                  <x-error field="temperatura_curado_recomendado" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="tiempo_curado_min" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Tiempo curado (días)
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-1">
                  <div class="justify-start grid grid-cols-3 gap-1">
                    <div class="pt-3 mr-3 text-sm text-gray-500 col-span-1">
                      min
                    </div>
                    <div class="mr-auto col-span-2 rounded-md shadow-sm">
                      <input
                        type="number"
                        id="tiempo_curado_min"
                        wire:model="tiempo_curado_min"
                        class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                        />
                      <x-error field="tiempo_curado_min" class="mt-2 text-xs italic text-red-500" />
                    </div>
                  </div>
                </div>
                <div class="mt-1 sm:mt-0 sm:col-span-1">
                  <div class="justify-start grid grid-cols-3 gap-1">
                    <div class="pt-3 mr-3 text-sm text-gray-500 col-span-1">
                      max
                    </div>
                    <div class="mr-auto col-span-2 rounded-md shadow-sm">
                      <input
                        type="number"
                        id="tiempo_curado_max"
                        wire:model="tiempo_curado_max"
                        class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                        />
                      <x-error field="tiempo_curado_max" class="mt-2 text-xs italic text-red-500" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:border-t sm:border-gray-200 sm:pt-5">
                <h3 class="block font-bold text-gray-700 text-md sm:mt-px sm:pt-2">
                  Etapas
                </h3 >
              </div>

              @foreach ($etapas as $e)
                @if ($loop->first)
                  <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:pt-5">
                @else
                  <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                @endif
                  <label for="vegetacion_interior_dias_bajo" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    {{ $e->nombre }} interior (días)
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-1">
                    <div class="justify-start grid grid-cols-3 gap-1">
                      <div class="pt-3 mr-3 text-sm text-gray-500 col-span-1">
                        pre
                      </div>
                      <div class="mr-auto col-span-2 rounded-md shadow-sm">
                        <input
                          type="number"
                          min="1"
                          wire:model="{{ 'dias_inicio.' . $e->nombre . '.dia_inicio_pre_interior' }}"
                          class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                          />
                        <x-error field="{{ 'dias_inicio.' . $e->nombre . '.dia_inicio_pre_interior' }}" class="mt-2 text-xs italic text-red-500" />
                      </div>
                    </div>
                  </div>
                  <div class="mt-1 sm:mt-0 sm:col-span-1">
                    <div class="justify-start grid grid-cols-3 gap-1">
                      <div class="pt-3 mr-3 text-sm text-gray-500 col-span-1">
                        etapa
                      </div>
                      <div class="mr-auto col-span-2 rounded-md shadow-sm">
                        <input
                          type="number"
                          min="1"
                          wire:model="{{ 'dias_inicio.' . $e->nombre . '.dia_inicio_etapa_interior' }}"
                          class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                          />
                        <x-error field="{{ 'dias_inicio.' . $e->nombre . '.dia_inicio_etapa_interior' }}" class="mt-2 text-xs italic text-red-500" />
                      </div>
                    </div>
                  </div>
                </div>

                <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="vegetacion_interior_dias_bajo" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    {{ $e->nombre }} exterior (días)
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-1">
                    <div class="justify-start grid grid-cols-3 gap-1">
                      <div class="pt-3 mr-3 text-sm text-gray-500 col-span-1">
                        pre
                      </div>
                      <div class="mr-auto col-span-2 rounded-md shadow-sm">
                        <input
                          type="number"
                          min="1"
                          wire:model="{{ 'dias_inicio.' . $e->nombre . '.dia_inicio_pre_exterior' }}"
                          class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                          />
                        <x-error field="{{ 'dias_inicio.' . $e->nombre . '.dia_inicio_pre_exterior' }}" class="mt-2 text-xs italic text-red-500" />
                      </div>
                    </div>
                  </div>
                  <div class="mt-1 sm:mt-0 sm:col-span-1">
                    <div class="justify-start grid grid-cols-3 gap-1">
                      <div class="pt-3 mr-3 text-sm text-gray-500 col-span-1">
                        etapa
                      </div>
                      <div class="mr-auto col-span-2 rounded-md shadow-sm">
                        <input
                          type="number"
                          min="1"
                          wire:model="{{ 'dias_inicio.' . $e->nombre . '.dia_inicio_etapa_exterior' }}"
                          class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                          />
                        <x-error field="{{ 'dias_inicio.' . $e->nombre . '.dia_inicio_etapa_exterior' }}" class="mt-2 text-xs italic text-red-500" />
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach

            </div>
          </div>
        </div>

        <div class="pt-5 mt-8">
          <div class="flex justify-end">
            <span class="inline-flex rounded-md shadow-sm">
              <button type="button" wire:click="cancel" class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md leading-5 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                Cancelar
              </button>
            </span>
            <span class="inline-flex ml-3 rounded-md shadow-sm">
              <button type="submit" form="create-tiposemilla" class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                Guardar
              </button>
            </span>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>
