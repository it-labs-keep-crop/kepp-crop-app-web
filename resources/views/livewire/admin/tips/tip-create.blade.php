<div class="container">
  <x-toast type="success" />

  <div class="pt-2 pb-6 md:py-6">
    <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
      <div class="md:flex md:items-center md:justify-between">
        <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
      </div>
    </div>
    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
      <div class="py-4">
        <form class="flex flex-col" id="create-tip" wire:submit.prevent="store">
          <div class="py-2 -my-2 overflow-x-auto sm:px-6 lg:-mx-6 lg:px-6">
            <div class="inline-block min-w-full overflow-hidden align-middle bg-white border-b border-gray-200 shadow sm:rounded-lg">
              <div class="mx-6">

                <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:pt-5">
                  <label for="titulo" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Titulo
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                      <input id="titulo" wire:model="titulo" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    </div>
                    <x-error field="titulo" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>

                <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="contenido" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Contenido
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                      <textarea rows="3" id="contenido" wire:model="contenido" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"> </textarea>
                    </div>
                    <x-error field="contenido" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>

                <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="link" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Link
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                      <input id="link" wire:model="link" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    </div>
                    <x-error field="link" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>

                <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="imagen" class="block text-sm font-medium text-gray-700 leading-5">
                    Imagen
                  </label>
                  <div class="mt-2 sm:mt-0 sm:col-span-2">
                    <div class="text-sm">
                      @if ($imagen)
                        <x-image-modal :path="$imagen->temporaryUrl()">
                          <img class="w-24 h-24 overflow-hidden rounded-md" src="{{ $imagen->temporaryUrl() }}">
                        </x-image-modal>
                      @else
                        <x-image-modal :path="asset('img/img.png')">
                          <img class="w-24 h-24 overflow-hidden rounded-md" src="{{ asset('img/img.png') }}">
                        </x-image-modal>
                      @endif
                    </div>

                    <div class="flex max-w-lg rounded-md shadow-sm" x-show="editable">
                      <input type="file" wire:model="imagen" x-ref="imagen">
                    </div>
                    <x-error field="imagen" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>

                <div class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:py-5">
                  <label for="etapa_id" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Aparece en el listado de Tips?
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="flex max-w-lg rounded-md shadow-sm">
                      <select id="listado" wire:model="listado" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                      </select>
                    </div>
                    <x-error field="listado" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>

                <div class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:py-5">
                  <label for="etapa_id" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Tipo
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="flex max-w-lg rounded-md shadow-sm">
                      <select id="tipable_type" wire:model="tipable_type" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                        <option value="0" disabled>Seleccione un tipo</option>
                        @foreach($tipos_label as $k => $v)
                          <option value="{{ $k }}">{{ $v }}</option>
                        @endforeach
                        <option value="">Genérico</option>
                      </select>
                    </div>
                    <x-error field="tipable_type" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>

                @if ($tipable_type)
                <div class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:py-5">
                  <label for="tipable_id" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    {{ $tipos_label[$tipable_type] }}
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="flex max-w-lg rounded-md shadow-sm">
                      <select id="tipable_id" wire:model="tipable_id" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                        <option value="0" disabled>Seleccione una opción</option>
                        @foreach($tipos[$tipable_type] as $v)
                          <option value="{{ $v['id'] }}">{{ $v['nombre'] }}</option>
                        @endforeach
                      </select>
                    </div>
                    <x-error field="tipable_id" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>
                @endif

              </div>
            </div>
          </div>

          <div class="pt-5 mt-8">
            <div class="flex justify-end">
              <span class="inline-flex rounded-md shadow-sm">
                <button
                  type="button"
                  wire:click="cancel"
                  class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md leading-5 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out"
                  >
                  Cancelar
                </button>
              </span>
              <span class="inline-flex ml-3 rounded-md shadow-sm">
                <button
                  type="submit"
                  form="create-tip"
                  class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out"
                  >
                  Guardar
                </button>
              </span>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
