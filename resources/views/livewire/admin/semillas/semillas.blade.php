<div class="container">
  <x-toast type="success" />

  <div class="pt-2 pb-6 md:py-6">
    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
      <div class="py-4">
        <div class="flex justify-between pb-4">
          <div class="flex w-2/3 mr-auto">
            <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
          </div>
          <div class="flex w-1/3">
            <span class="ml-auto shadow-sm rounded-md">
              <a
                type="button"
                href="{{ route('semillas.create') }}"
                class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:shadow-outline-idigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out"
                >
                Crear semilla
              </a >
            </span>
          </div>
        </div>

        <livewire:semilla-table />

      </div>
    </div>
  </div>
</div>
