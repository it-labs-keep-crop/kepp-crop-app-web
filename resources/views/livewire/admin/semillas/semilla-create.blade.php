<div class="pt-2 pb-6 md:py-6">
  <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
    <div class="md:flex md:items-center md:justify-between">
      <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
    </div>
  </div>
  <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
    <div class="py-4">
      <form class="flex flex-col" id="edit-user" wire:submit.prevent="store">
        <div class="py-2 -my-2 overflow-x-auto sm:px-6 lg:-mx-6 lg:px-6">
          <div class="inline-block min-w-full overflow-hidden align-middle bg-white border-b border-gray-200 shadow sm:rounded-lg">
            <div class="mx-6">
              <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:pt-5">
                <label for="nombre" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Nombre
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <input id="nombre" wire:model="nombre" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                  </div>
                  <x-error field="nombre" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="publicado" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Publicado
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <select id="publicado" wire:model="publicado" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                      <option value="0">Borrador</option>
                      <option value="1">Publicado</option>
                    </select>
                  </div>
                  <x-error field="publicado" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="tipo_semilla_id" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Tipo semilla
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <select id="tipo_semilla_id" wire:model="tipo_semilla_id" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                      <option value="0" disabled>Seleccione un tipo</option>
                      @foreach($tiposSemilla as $tipo)
                        <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                      @endforeach
                    </select>
                  </div>
                  <x-error field="tipo_semilla_id" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="marca" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Marca
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                    <select id="marca" wire:model="marca" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                      <option value="" disabled>Seleccione una marca</option>
                      @foreach($marcas as $m)
                        <option value="{{ $m->id }}">{{ $m->nombre }}</option>
                      @endforeach
                    </select>
                  </div>
                  <x-error field="marca" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="thc_porcentaje" class="block text-sm font-medium text-gray-700 leading-5">
                  % THC
                </label>
                <div class="mt-2 sm:mt-0 sm:col-span-2">
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="max-w-lg rounded-md shadow-sm sm:max-w-xs">
                      <input id="thc_porcentaje" wire:model="thc_porcentaje" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    </div>
                    <x-error field="thc_porcentaje" class="mt-2 text-xs italic text-red-500" />
                  </div>
                </div>
              </div>

              <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                <label for="caracteristicas" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                  Características
                </label>
                <div class="mt-1 sm:mt-0 col-span-2">
                  <div class="max-w-lg shadow-sm" x-show="editable">
                    <x-inputs.rich-text
                        wire:model.debounce.500ms="caracteristicas"
                        initValue="{{ $caracteristicas }}"
                        >
                    </x-inputs.rich-text>
                  </div>
                  <x-error field="caracteristicas" class="mt-2 text-xs italic text-red-500" />
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="pt-5 mt-8">
          <div class="flex justify-end">
            <span class="inline-flex rounded-md shadow-sm">
              <button type="button" wire:click="cancel"
                class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md leading-5 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                Cancelar
              </button>
            </span>
            <span class="inline-flex ml-3 rounded-md shadow-sm">
              <button type="submit" form="edit-user"
                class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                Guardar
              </button>
            </span>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>
