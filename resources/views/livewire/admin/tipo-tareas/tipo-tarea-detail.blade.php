<div class="container">

  <x-toast type="success" />
  <x-delete-modal>
    <x-slot name="title">Confirmación</x-slot>
    <x-slot name="message">¿Está seguro de eliminar el tipo de tarea?</x-slot>
  </x-delete-modal>

  <div class="pt-2 pb-6 md:py-6" x-data="{editable: false, modal: false, variables: false}" x-cloak x-on:stop-edit.window="editable = false">
    <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
      <div class="md:flex md:items-center md:justify-between">
        <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
        <div class="flex mt-4 md:mt-0 md:ml-4" x-show="!editable">
          <span class="shadow-sm rounded-md">
            <button
              type="button" wire:click="$emit('open-modal')"
              class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-red-700 border border-gray-300 leading-5 rounded-md hover:bg-red-600 focus:outline-none focus:shadow-outline-blue focus:border-red-700 active:text-white active:bg-red-700 transition duration-150 ease-in-out"
              >
              Eliminar
            </button>
          </span>
          <span class="ml-3 shadow-sm rounded-md">
            <button
              type="button" @click="editable = true; variables = true"
              class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out"
              >
              Editar
            </button>
          </span>
        </div>
      </div>
    </div>
    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
      <div class="py-4">
        <form class="flex flex-col" id="edit-tipotarea" wire:submit.prevent="store">
          <div class="py-2 -my-2 overflow-x-auto sm:px-6 lg:-mx-6 lg:px-6">
            <div class="inline-block min-w-full overflow-hidden align-middle bg-white border-b border-gray-200 shadow sm:rounded-lg">
              <div class="mx-6">
                <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:pt-5">
                  <label for="titulo" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Titulo
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="max-w-lg rounded-md shadow-sm" x-show="editable">
                      <input id="titulo" wire:model="titulo" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    </div>
                    <x-error field="titulo" class="mt-2 text-xs italic text-red-500" />
                    <p class="text-m" x-show="!editable">
                      {{ $tipo->titulo }}
                    </p>
                  </div>
                </div>

                <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="horario" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Horario (24h)
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-1">
                    <div class="justify-start grid grid-cols-3 gap-1">
                      <div class="pt-3 mr-3 text-sm text-gray-500 col-span-1">
                        min
                      </div>
                      <div class="mr-auto col-span-2 rounded-md shadow-sm">
                        <input
                          type="number"
                          id="horario_min"
                          min="0"
                          max="23"
                          wire:model="horario_min"
                          class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                          x-show="editable"
                          />
                        <x-error field="horario_min" class="mt-2 text-xs italic text-red-500" />
                        <p class="mt-2 text-m" x-show="!editable">
                          {{ $tipo->horario_min }}:00
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="mt-1 sm:mt-0 sm:col-span-1">
                    <div class="justify-start grid grid-cols-3 gap-1">
                      <div class="pt-3 mr-3 text-sm text-gray-500 col-span-1">
                        max
                      </div>
                      <div class="mr-auto col-span-2 rounded-md shadow-sm">
                        <input
                          type="number"
                          id="horario_max"
                          min="0"
                          max="23"
                          wire:model="horario_max"
                          class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                          x-show="editable"
                          />
                        <x-error field="horario_max" class="mt-2 text-xs italic text-red-500" />
                        <p class="mt-2 text-m" x-show="!editable">
                          {{ $tipo->horario_max }}:00
                        </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="my-6 sm:my-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="descripcion" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Template
                  <button
                    type="button"
                    @click="variables = !variables"
                    class="mx-3 text-sm font-light text-indigo-600 hover:text-indigo-500 transition focus:outline-none duration-150 ease-in-out">
                    variables
                  </button>
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="flex descripcion-w-lg rounded-md shadow-sm" x-show="editable">
                      <textarea rows="3" id="template" wire:model="template" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"></textarea>
                    </div>
                    <x-error field="template" class="mt-2 text-xs italic text-red-500" />
                    <p class="text-sm" x-show="!editable">
                      {{ $tipo->template }}
                    </p>
                  </div>
                </div>

                <div x-show="variables" class="mt-2 mb-5 overflow-hidden bg-white rounded-lg shadow">
                  <div class="flex flex-row inline-block px-4 pt-4 pb-2 mx-4">
                    <x-go-info-24 class="w-6 h-6 mr-4 text-indigo-500"/>
                      <p class="mt-1 text-sm text-gray-700 leading-5">
                      Puede usar variables en el texto, serán reemplazadas automáticamente al generar las tareas.
                      </p>
                  </div>
                  <div class="px-4 py-4 sm:p-6 grid md:grid-cols-2 lg:grid-cols-3 gap-1">
                    @foreach($tipo->variables as $key => $value)
                      <div class="flex-1 px-4 py-1 text-sm leading-5">
                        <div class="flex flex-row py-1 pr-5">
                          <button
                            @click="document.getElementById(".{{$key}}.").select(); document.execCommand('copy');"
                            type="button"
                            class="font-medium text-indigo-700 cursor-pointer select-all focus:outline-none">
                            {{ '${'. $key .'}' }}
                          </button>
                          <input type="hidden" id="{{$key}}" value="{{ '${'.$key.'}' }}">
                        </div>
                        <p class="text-xs font-light text-gray-500">{{ $value }}</p>
                      </div>
                    @endforeach
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div class="pt-5 mt-8 border-t border-gray-200" x-show="editable">
            <div class="flex justify-end">
              <span class="inline-flex rounded-md shadow-sm">
                <button
                  type="button" wire:click="cancel"
                  class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md leading-5 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out"
                  >
                  Cancelar
                </button>
              </span>
              <span class="inline-flex ml-3 rounded-md shadow-sm">
                <button
                  type="submit"
                  form="edit-tipotarea"
                  class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out"
                  >
                  Guardar
                </button>
              </span>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
