<div class="container">
  <x-toast type="success" />

  <div class="pt-2 pb-6 md:py-6">
    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
      <div class="py-4">
        <div class="flex justify-between pb-4">
          <div class="flex w-2/3 mr-auto">
            <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
          </div>
        </div>

        <livewire:tipo-tarea-table />

      </div>
    </div>
  </div>
</div>
