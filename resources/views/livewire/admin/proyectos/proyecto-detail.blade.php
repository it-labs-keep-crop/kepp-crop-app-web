<div class="container">
  <x-toast type="success" />
  <x-delete-modal>
    <x-slot name="title">Confirmación</x-slot>
    <x-slot name="message">¿Está seguro de eliminar el proyecto?</x-slot>
  </x-delete-modal>

  <div class="pt-2 pb-6 md:py-6" x-data="{editable: false, modal: false}" x-cloak x-on:stop-edit.window="editable = false">
    <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
      <div class="md:flex md:items-center md:justify-between">
        <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
        <div class="flex mt-4 md:mt-0 md:ml-4" x-show="!editable">
          <span class="shadow-sm rounded-md">
            <button type="button" wire:click="$emit('open-modal')"
                    class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-red-700 border border-gray-300 leading-5 rounded-md hover:bg-red-600 focus:outline-none focus:shadow-outline-blue focus:border-red-700 active:text-white active:bg-red-700 transition duration-150 ease-in-out">
              Eliminar
            </button>
          </span>
          <span class="ml-3 shadow-sm rounded-md">
            <button type="button" @click="editable = true"
                    class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
              Editar
            </button>
          </span>
        </div>
      </div>
    </div>
    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
      <div class="py-4">
        <form class="flex flex-col" id="edit-proyecto" wire:submit.prevent="store">
          <div class="py-2 -my-2 overflow-x-auto sm:px-6 lg:-mx-6 lg:px-6">
            <div class="inline-block min-w-full overflow-hidden align-middle bg-white border-b border-gray-200 shadow sm:rounded-lg">
              <div class="mx-6">
                <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:pt-5">
                  <label for="nombre" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Nombre
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="max-w-lg rounded-md shadow-sm" x-show="editable">
                      <input id="nombre" wire:model="nombre" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    </div>
                    @error('nombre')
                    <p class="mt-2 text-xs italic text-red-500">{{ $message }}</p>
                  @enderror
                  <p class="text-sm" x-show="!editable">
                  {{ $nombre }}
                  </p>
                  </div>
                </div>

                <div class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="tipo_cultivo" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Tipo cultivo
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="flex max-w-lg rounded-md shadow-sm" x-show="editable">
                      <input id="tipo_cultivo" wire:model="tipo_cultivo" class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                    </div>
                    @error('tipo_cultivo')
                    <p class="mt-2 text-xs italic text-red-500">{{ $message }}</p>
                  @enderror
                  <p class="text-sm" x-show="!editable">
                  {{ $tipo_cultivo }}
                  </p>
                  </div>
                </div>

                <div class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="tipo_semilla_id" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Tipo semilla
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="flex max-w-lg rounded-md shadow-sm" x-show="editable">
                      <select id="tipo_semilla_id" wire:model="tipo_semilla_id" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                        @foreach($tiposSemilla as $tipo)
                          <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                        @endforeach
                      </select>
                    </div>
                    @error('tipo_semilla_id')
                    <p class="mt-2 text-xs italic text-red-500">{{ $message }}</p>
                  @enderror
                  <p class="text-sm" x-show="!editable">
                  {{ $proyecto->tipoSemilla->nombre }}
                  </p>
                  </div>
                </div>

                <div class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="etapa_id" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Etapa
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="flex max-w-lg rounded-md shadow-sm" x-show="editable">
                      <select id="etapa_id" wire:model="etapa_id" class="block w-full form-select transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                        @foreach($etapas as $etapa)
                          <option value="{{ $etapa->id }}">{{ $etapa->nombre }}</option>
                        @endforeach
                      </select>
                    </div>
                    @error('etapa_id')
                    <p class="mt-2 text-xs italic text-red-500">{{ $message }}</p>
                  @enderror
                  <p class="text-sm" x-show="!editable">
                  {{ $proyecto->etapa->nombre }}
                  </p>
                  </div>
                </div>

                <div class="my-6 sm:my-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                  <label for="user" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Usuario
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <p class="text-sm">
                    {{ $proyecto->user->name }}
                    </p>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div class="pt-5 mt-8 border-t border-gray-200" x-show="editable">
            <div class="flex justify-end">
              <span class="inline-flex rounded-md shadow-sm">
                <button type="button" wire:click="cancel"
                        class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md leading-5 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                  Cancelar
                </button>
              </span>
              <span class="inline-flex ml-3 rounded-md shadow-sm">
                <button type="submit" form="edit-proyecto"
                                      class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                  Guardar
                </button>
              </span>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
