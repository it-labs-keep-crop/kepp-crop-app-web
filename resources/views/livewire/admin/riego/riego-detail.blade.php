<div>
  <x-toast type="success" />

  <div class="pt-2 pb-6 md:py-6" x-data="{editable: false, modal: false}" x-cloak x-on:stop-edit.window="editable = false">
    <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
      <div class="md:flex md:items-center md:justify-between">
        <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
        <div class="flex mt-4 md:mt-0 md:ml-4" x-show="!editable">
          <span class="ml-3 shadow-sm rounded-md">
            <button type="button" @click="editable = true"
                    class="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">
              Editar
            </button>
          </span>
        </div>
      </div>
    </div>
    <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
      <div class="py-4">
        <form class="flex flex-col" id="edit-regado" wire:submit.prevent="store">
          <div class="py-2 -my-2 overflow-x-auto sm:px-6 lg:-mx-6 lg:px-6">
            <div class="inline-block min-w-full overflow-hidden align-middle bg-white border-b border-gray-200 shadow sm:rounded-lg">
              <div class="mx-6">
                <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-gray-200 sm:pt-5">
                  <label for="nombre" class="block text-sm font-medium text-gray-700 leading-5 sm:mt-px sm:pt-2">
                    Tipo de semilla
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <p class="text-m">
                    {{ $tipoSemilla->nombre }}
                    </p>
                  </div>
                </div>

                <div class="mt-6 mb-6 sm:mb-5 sm:mt-5 sm:grid sm:grid-cols-5 sm:gap-4 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="justify-start grid grid-cols-3 gap-1">
                      <div class="pt-3 mr-3 text-sm font-medium text-gray-700 col-span-1">
                        Regar cada
                      </div>
                      <div class="mr-auto col-span-1 rounded-md shadow-sm">
                        <input
                          x-show="editable"
                          min="1"
                          type="number"
                          id="frecuencia_regado"
                          wire:model="frecuencia_regado"
                          class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                          />
                        <x-error field="frecuencia_regado" class="mt-2 text-xs italic text-red-500" />
                          <p class="pt-3 text-gray-700 text-m" x-show="!editable">
                          {{ $frecuencia_regado }}
                          </p>
                      </div>
                      <div class="pt-3 ml-3 text-sm font-medium text-gray-700 col-span-1">
                        días
                      </div>
                    </div>
                  </div>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <div class="justify-start grid grid-cols-3 gap-1">
                      <div class="pt-3 mr-3 text-sm font-medium text-gray-700 col-span-1">
                        Fertilizar cada
                      </div>
                      <div class="mr-auto col-span-1 rounded-md shadow-sm">
                        <input
                          x-show="editable"
                          min="0"
                          type="number"
                          id="frecuencia_fertilizante"
                          wire:model="frecuencia_fertilizante"
                          class="block w-full form-input transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                          />
                        <x-error field="frecuencia_fertilizante" class="mt-2 text-xs italic text-red-500" />
                          <p class="pt-3 text-gray-700 text-m" x-show="!editable">
                          {{ $frecuencia_fertilizante }}
                          </p>
                      </div>
                      <div class="pt-3 ml-3 text-sm font-medium text-gray-700 col-span-1">
                        riegos
                      </div>
                    </div>
                  </div>
                </div>

                <div class="my-6 sm:my-5 sm:items-center sm:border-t sm:border-gray-200 sm:pt-5">

                  <div class="max-w-md mx-auto my-3 border border-solid rounded border-grey-light shadow-sm">
                    <div class="px-2 py-2 text-center text-white bg-indigo-400 border-b border-solid border-grey-light">
                      {{ $today->format('F Y') }}
                    </div>
                    <div class="">

                      <table class="w-full">

                        @if ($loading && $frecuencia_regado <= 0)
                          <tr class="border-b">
                            <th class="px-4 py-3">Cargando</th>
                          </tr>
                          <tr class="h-32">
                            <td class="py-10 cursor-wait grid grid-cols-3">
                              <div class="mx-auto col-start-2">
                                <x-fas-spinner class="w-12 h-12 text-indigo-400 animate-spin"/>
                              </div>
                            </td>
                          </tr>
                        @else

                          <tr class="border-b">
                            <th class="px-4 py-3">L</th>
                            <th class="px-4 py-3">M</th>
                            <th class="px-4 py-3">M</th>
                            <th class="px-4 py-3">J</th>
                            <th class="px-4 py-3">V</th>
                            <th class="px-4 py-3">S</th>
                            <th class="px-4 py-3">D</th>
                          </tr>

                          @while ($dayCounter < 35)
                            <tr>
                              @for ($i = 0; $i < 7; $i++)
                                @if ($frecuencia_fertilizante > 0 && ($dayCounter % ($frecuencia_regado * $frecuencia_fertilizante) == 0))
                                  <td class="px-4 py-3 text-center text-white bg-orange-300 cursor-not-allowed">
                                @elseif (($dayCounter + $frecuencia_regado) % $frecuencia_regado == 0)
                                  <td class="px-4 py-3 text-center text-white bg-indigo-400 cursor-not-allowed">
                                @else
                                  <td class="px-4 py-3 text-center cursor-not-allowed">
                                @endif
                                {{ $tempDate->day }}
                                  </td>
                                  @php
                                    $tempDate->addDay();
$dayCounter ++;
@endphp
                          @endfor
                            </tr>
                          @endwhile

                        @endif
                      </table>
                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>

          <div class="pt-5 mt-8 border-t border-gray-200" x-show="editable">
            <div class="flex justify-end">
              <span class="inline-flex rounded-md shadow-sm">
                <button type="button" wire:click="cancel"
                        class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md leading-5 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                  Cancelar
                </button>
              </span>
              <span class="inline-flex ml-3 rounded-md shadow-sm">
                <button type="submit" form="edit-regado"
                                      class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent leading-5 rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                  Guardar
                </button>
              </span>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
