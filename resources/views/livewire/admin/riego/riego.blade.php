<x-toast type="success" />

<div class="pt-2 pb-6 md:py-6">
  <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
    <div class="md:flex md:items-center md:justify-between">
      <x-breadcrumb :breadcrumb="$breadcrumb" class="flex-1"></x-breadcrumb>
    </div>
  </div>
  <div class="px-4 mx-auto max-w-7xl sm:px-6 md:px-8">
    <div class="py-4">

      <div class="flex justify-between pb-4">
        <div class="flex w-full mr-auto">
          <div class="relative w-full mt-1 rounded-md shadow-sm">
            <input id="search" wire:model='q' class="block w-full pr-10 form-input sm:text-sm sm:leading-5" placeholder="Buscar">
            <div class="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
              <x-css-search class="text-gray-400" />
            </div>
          </div>
        </div>
      </div>

      <div class="flex flex-col">
        <div class="my-2 overflow-x-auto border-b border-gray-200 rounded-lg">
          {{-- <div class="inline-block w-full align-middle sm:rounded-lg"> --}}
          <table class="w-full border-b border-gray-200 rounded-lg table-fixed divide-y divide-gray-200">
            <thead>
              <tr>
                <th class="w-2/5 py-3 pl-6 text-xs font-medium tracking-wider text-left text-gray-500 uppercase break-words whitespace-normal border-b border-gray-200 bg-gray-50 leading-4">
                  Tipo semilla
                </th>
                <th class="w-1/5 px-3 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase break-words whitespace-normal border-b border-gray-200 bg-gray-50 leading-4">
                  Frecuencia de regado
                </th>
                <th class="w-1/5 px-3 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase break-words whitespace-normal border-b border-gray-200 bg-gray-50 leading-4">
                  Frecuencia de fertilizante
                </th>
                <th class="w-1/5 py-3 pr-3 border-b border-gray-200 bg-gray-50"></th>
              </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
              @foreach ($tipos as $tipo)
              <tr class="hover:bg-gray-100">
                <td class="px-6 py-4 text-sm font-medium text-gray-900 whitespace-no-wrap leading-5">
                  {{ $tipo->nombre }}
                </td>
                <td class="px-6 py-4 text-sm text-gray-700 whitespace-no-wrap leading-5">
                  Cada {{ $tipo->frecuencia_regado}} días
                </td>
                <td class="px-6 py-4 text-sm text-gray-700 whitespace-no-wrap leading-5">
                  Cada {{ $tipo->frecuencia_fertilizante }} riegos
                </td>
                <td class="px-6 py-4 text-sm font-medium text-right whitespace-no-wrap border-b border-gray-200 leading-5">
                  <a
                    href="{{ route('regado.detail', ['tipoSemilla' => $tipo->id]) }}"
                    class="text-indigo-500 hover:text-indigo-700"
                    >
                    Ver
                  </a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        {{ $tipos->links() }}
      </div>
    </div>
  </div>
</div>
