@extends('layouts.base')

@section('body')
  <div>
    @yield('main')
  </div>
@endsection
