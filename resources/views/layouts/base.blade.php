<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @hasSection('title')

      <title>@yield('title') - {{ config('app.name') }}</title>
    @else
      <title>{{ config('app.name') }}</title>
    @endif

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url(asset('favicon.ico')) }}">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ url(mix('css/app.css')) }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >
    <livewire:styles />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Head Scripts -->
    @stack('head-scripts')
    @bukStyles(true)
  </head>

  <body>
    @yield('body')

    <!-- Bottom Scripts -->
    <livewire:scripts />
    @stack('bottom-scripts')
    <script src="{{ url(mix('js/app.js')) }}"></script>
    @bukScripts(true)
  </body>
</html>
