@extends('layouts.base')

@section('body')

  <div class="flex h-screen overflow-hidden bg-gray-100">
    <!-- Off-canvas menu for mobile -->
    <div class="md:hidden">
      <div class="fixed inset-0 z-40 flex">
        <!--
          Off-canvas menu overlay, show/hide based on off-canvas menu state.

          Entering: "transition-opacity ease-linear duration-300"
          From: "opacity-0"
          To: "opacity-100"
          Leaving: "transition-opacity ease-linear duration-300"
          From: "opacity-100"
          To: "opacity-0"
        -->
        <div class="fixed inset-0">
          <div class="absolute inset-0 bg-gray-600 opacity-75"></div>
        </div>
        <!--
          Off-canvas menu, show/hide based on off-canvas menu state.

          Entering: "transition ease-in-out duration-300 transform"
          From: "-translate-x-full"
          To: "translate-x-0"
          Leaving: "transition ease-in-out duration-300 transform"
          From: "translate-x-0"
          To: "-translate-x-full"
        -->
        <div class="relative flex flex-col flex-1 w-full max-w-xs bg-gray-800">
          <div class="absolute top-0 right-0 p-1 -mr-14">
            <button class="flex items-center justify-center w-12 h-12 rounded-full focus:outline-none focus:bg-gray-600" aria-label="Close sidebar">
              <svg class="w-6 h-6 text-white" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
              </svg>
            </button>
          </div>
          <div class="flex-1 h-0 pt-5 pb-4 overflow-y-auto">
            <div class="flex items-center flex-shrink-0 px-4">
              <h1 class="text-lg text-white">
                KeepCrop Logo
              </h1>
              {{-- <img class="w-auto h-8" src="{{asset('img/logo_indigo.png')}}" alt="Keep Crop"> --}}
            </div>
            <nav class="px-2 mt-5 space-y-1">
              {{-- $menu from app/Providers/AppServiceProvider.php (View Composer) --}}
              @foreach ($menu as $label => $items)
                <h3 class="flex items-center px-2 py-2 text-xs font-medium text-gray-100 uppercase focus group leading-5 rounded-md">{{ $label }}</h3>
                @foreach ($items as $item)
                  <a href="{{ $item['url'] }}" class="flex items-center px-2 py-2 text-base font-medium text-gray-300 group leading-6 rounded-md hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">
                    @svg($item['icon'], [
                    'class' => 'w-6 h-6 mr-4 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150'
                    ])
                    {{ $item['label'] }}
                  </a>
                @endforeach
              @endforeach
            </nav>
          </div>
          <div class="flex flex-shrink-0 p-4 bg-gray-700">
            <a href="#" class="flex-shrink-0 block group">
              <div class="flex items-center">
                <div>
                  <img class="inline-block w-10 h-10 rounded-full" src="{{asset('img/user.png')}}" alt="">
                </div>
                <div class="ml-3">
                  <p class="text-base font-medium text-white leading-6">
                  {{ Auth::user()->name }}
                  </p>
                  <x-form-button
                    :action="route('logout')"
                    method="POST"
                    class="text-xs font-medium text-gray-300 leading-4 group-hover:text-gray-200"
                    >
                    Cerrar sesión
                  </x-form-button>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="flex-shrink-0 w-14">
          <!-- Force sidebar to shrink to fit close icon -->
        </div>
      </div>
    </div>

    <!-- Static sidebar for desktop -->
    <div class="hidden md:flex md:flex-shrink-0">
      <div class="flex flex-col w-64">
        <!-- Sidebar component, swap this element with another sidebar if you like -->
        <div class="flex flex-col flex-1 h-0 bg-gray-800">
          <div class="flex flex-col flex-1 pt-5 pb-4 overflow-y-auto">
            <div class="flex items-center flex-shrink-0 px-4">
              {{-- <img class="w-auto h-8" src="{{asset('img/logo_indigo.png')}}" alt="KeepCrop"> --}}
              <h1 class="text-lg font-bold text-white">
                KeepCrop Logo
              </h1>
            </div>
            <nav class="flex-1 px-2 mt-5 bg-gray-800 space-y-1">
              {{-- $menu from app/Providers/AppServiceProvider.php (View Composer) --}}
              @foreach ($menu as $label => $items)
                <h3 class="flex items-center px-2 py-2 text-xs font-medium text-gray-100 uppercase focus group leading-5 rounded-md">{{ $label }}</h3>
                @foreach ($items as $item)
                  <a href="{{ $item['url'] }}" class="flex items-center px-2 py-2 text-sm font-medium text-gray-300 focus group leading-5 rounded-md active:text-white active:bg-gray-900 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">
                    @svg($item['icon'], [
                    'class' => 'w-6 h-6 mr-4 text-gray-400 group-hover:text-gray-300 group-focus:text-gray-300 transition ease-in-out duration-150'
                    ])
                    {{ $item['label'] }}
                  </a>
                @endforeach
              @endforeach
            </nav>
          </div>
          <div class="flex flex-shrink-0 p-4 bg-gray-700">
            <a href="#" class="flex-shrink-0 block w-full group">
              <div class="flex items-center">
                <div>
                  @if (Auth::user()->avatar)
                    <img class="inline-block rounded-full h-9 w-9" src="{{ asset(Auth::user()->avatar) }}" alt="">
                  @else
                    <img class="inline-block rounded-full h-9 w-9" src="{{asset('img/user.png')}}" alt="">
                  @endif
                </div>
                <div class="ml-3">
                  <p class="text-sm font-medium text-white leading-5">
                  {{ Auth::user()->name }}
                  </p>
                  <x-form-button
                    :action="route('logout')"
                    method="POST"
                    class="text-xs font-medium text-gray-300 leading-4 group-hover:text-gray-200 focus:outline-none focus:text-white"
                    >
                    Cerrar sesión
                  </x-form-button>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="flex flex-col flex-1 w-0 overflow-hidden">
      <div class="pt-1 pl-1 md:hidden sm:pl-3 sm:pt-3">
        <button class="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:bg-gray-200 transition ease-in-out duration-150" aria-label="Open sidebar">
          <svg class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
          </svg>
        </button>
      </div>
      <main class="relative z-0 flex-1 overflow-y-auto bg-indigo-100 focus:outline-none" tabindex="0">
        {{ $slot }}
      </main>
    </div>
  </div>

@endsection
