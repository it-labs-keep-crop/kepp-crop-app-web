<div>
  @if (session()->has($type) && ! empty(session()->get($type)))
    <div x-data="{ show: false }" x-cloak x-init="setTimeout(() => {show = true}, 500); setTimeout(() => {show = false}, 3500)">
      <div
        class="fixed inset-0 z-20 flex items-end justify-center px-4 py-6 pointer-events-none sm:p-6 sm:items-start sm:justify-end"
        x-show="show"
        x-transition:enter="transition ease-out duration-700"
        x-transition:enter-start="opacity-0 transform scale-80"
        x-transition:enter-end="opacity-100 transform scale-100"
        x-transition:leave="transition ease-in duration-300"
        x-transition:leave-start="opacity-100 transform scale-100"
        x-transition:leave-end="opacity-0 transform scale-80">
        <div class="w-full max-w-sm bg-white rounded-lg shadow-lg pointer-events-auto">
          <div class="overflow-hidden rounded-lg shadow-xs">
            <div class="p-4">
              <div class="flex items-start">

                <div class="flex-shrink-0">
                  {!! $icons[$type] !!}
                </div>

                <div class="ml-3 w-0 flex-1 pt-0.5">
                  <p class="text-sm font-medium text-gray-900 leading-5">
                  Realizado
                  </p>
                  <p class="mt-1 text-sm text-gray-500 leading-5">
                  {{ session()->get($type) }}
                  </p>
                </div>

                <div class="flex flex-shrink-0 ml-4">
                  <button
                    @click="show = false"
                    class="inline-flex text-gray-400 focus:outline-none focus:text-gray-500 transition ease-in-out duration-150"
                    >
                    <svg class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                      <path
                        fill-rule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clip-rule="evenodd" />
                    </svg>
                  </button>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endif
</div>
