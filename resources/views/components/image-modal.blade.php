<div class="" x-cloak x-data="{showModalImage: false}">
  <div @click="showModalImage = true" class="inline-block w-auto cursor-pointer">
    {{ $slot }}
  </div>
  <div class="fixed inset-0 overflow-y-auto z-100" x-show="showModalImage">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
      <div class="fixed inset-0 transition-opacity"  x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
        x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0">
        <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
      </div>
      {{-- <span class="hidden h-screen align-middle sm:inline-block"></span>&#8203; --}}
      <div class="inline-block p-6 px-4 pt-5 pb-4 my-8 overflow-auto text-left align-middle bg-white rounded-lg shadow-xl transform transition-all"
        role="dialog" aria-modal="true" aria-labelledby="modal-headline"
        x-show="showModalImage"
        @click.away="showModalImage = false"
        x-transition:enter="ease-out duration-300"
        x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
        x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
        x-transition:leave="ease-in duration-200"
        x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
        x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95">
        <div class="absolute top-0 right-0 block pt-4 pr-4">
          <button @click="showModalImage=false"
          type="button"
          class="text-gray-400 hover:text-gray-500 focus:outline-none focus:text-gray-500 transition ease-in-out duration-150"
          aria-label="Close">
            <svg class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
          </button>
        </div>
        <div class="flex items-start">
          <div class="flex justify-center">
            <img class="" src="{{ asset($path) }}">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
