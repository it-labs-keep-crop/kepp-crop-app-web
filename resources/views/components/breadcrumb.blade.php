<div>
  <nav class="font-bold text-black" aria-label="Breadcrumb">
    <ol class="inline-flex p-0 list-none">
      @foreach ($breadcrumb as $route => $label)
      <li class="flex items-center">
        @if (!$loop->last)
          <a class="text-xl font-bold text-gray-900 leading-7 sm:text-xl sm:leading-9 " href="{{$route}}">{{$label}}</a>
          @svg($icons['separator'], [
            'class' => 'w-6 h-6 mx-3 text-gray-500 fill-current'
          ])

        @else
          <span class="text-xl font-bold text-gray-700" aria-current="page">{{$label}}</span>
        @endif
      </li>
      @endforeach
    </ol>
  </nav>
</div>
