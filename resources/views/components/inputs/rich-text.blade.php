<div
      class="rounded-md"
      {{ $attributes }}
      wire:ignore
      >
      <trix-editor
        x-ref="trix"
        input="x"
        class="block w-full form-textarea transition duration-150 ease-in-out sm:text-sm sm:leading-5"
        id="trix-editor"
        >
      </trix-editor>
        <input
          id="x"
          value="{!! $initValue !!}"
          type="hidden"
          name="{{ $attributes->wire('model')->value() }}"
          />
</div>
